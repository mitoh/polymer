/**
 * Variables para topicos
 */
var mqtt = require('mqtt'),
    protocol    = 'mqtt',
    host        = '192.168.244.10',
    username    = "admin",
    password    = "admin", 
    clientId    = "AuditNodeService",
    topicPath   = 'auditoria/#'

    

    function conectarATopico(topico){
        var client;
        console.log("Conectando a topico: " + topico)
        client  = mqtt.connect(protocol + '://' + host,{username:username, password:password, clientId:clientId})

        client.on('connect', function () {
            console.log("conectado a host: " + host);
            console.log("Conectado a topico: " + topico);
            client.subscribe(topico)
            //client.publish('auditoria/proceso/homologado/info', 'Hello mqtt')
        })
        
        client.on('message', function (topic, message, otro, otroo) {
            // message is Buffer
            console.log("Mensaje desde topico: " + message.toString())
            var sendToPlymer = {
                type: "message",
                message: message.toString(),
                date: new Date()
            }
            enviaraPolymer(client, JSON.stringify(sendToPlymer))
            //client.end()
        })
        
        client.on('packetreceive', function (topic, message, otro, otroo) {
            // message is Buffer
            console.log()
            //client.end()
        })
    }

    function desconectarTopico(){
        try{
            console.log("Finalizando conexion a topico.")
            client.end();
        }catch(err){
            console.log("Error tratando de desconectar: " + err)
        }
    }

/**
 * Variables para websocket
 */
const   http = require('http'),
        WebSocket = require('ws');

var webSocketPort = 8085

const server = new http.createServer(function(req, res){
    keepAlive:true
});

const wss = new WebSocket.Server({ server });

wss.on('connection', function connection(ws) {
    console.log('Conexion creada');
    //Auditar

    ws.on('message', function incoming(message) {
        message = JSON.parse(message)
        console.log('received: ', message);

        var action = message.action
        var value = message.value 

        console.log("Action :" + action)
        console.log("value: " + value)

        if(action == "toConnect"){
            if(value){
                conectarATopico(value);
            }else{
                "No hay valor para la conexion al topico"
            }
        }else if(action == "forSearch"){
            if(value){
                hacerBusqueda(ws, value)
            }else{
                console.error("No hay valor para la busqueda.")
            }
        }

        //Auditar
    },
    ws.on('close', function close(ws){
        console.log('Se detecta conexion WS cerrada');
        desconectarTopico();
        console.log('Conexion topico realizada. WS cerrado!');
        //Auditar
    }));

    var response = {
        type: "alert",
        message: "Conexión creada. Recibiendo streaming de Auditoría",
        date : new Date()
    }
    ws.send(JSON.stringify(response));
});


function hacerBusqueda(ws, busqueda){
    console.log("Buscando con los siguientes filtros: " + busqueda)

    if(!ws.busqueda){
        console.log("Seteando contenido de buqueda en ws")
        var doc1 = {
            "id": 3,
            "title": "Oracle released its latest database Oracle 12g",
            "body": "Yestaday Oracle has released its new database Oracle 12g, this would make more money for this company and lead to a nice profit report of annual year."
        }
    
        var doc2 = {
            "id": 4,
            "title": "Oracle released its profit report of 2015",
            "body": "As expected, Oracle released its profit report of 2015, during the good sales of database and hardware, Oracle's profit of 2015 reached 12.5 Billion."
        }

        ws.busqueda = []
        ws.busqueda.push(doc1)
        ws.busqueda.push(doc2)
    }

    var elasticlunr = require('elasticlunr');

    var index = elasticlunr(function () {
        this.addField('title')
        this.addField('body')
        this.setRef('id')
    });

    if(ws.busqueda){
        ws.busqueda.forEach(json => {
            index.addDoc(json)
        });
    }else{
        console.error("NO SE HA SETEADO EL CONTENIDO DE LA BUSQUEDA!!!")
    }

    //var sear =  index.search("Oracle database profit");
    var sear =  index.search(busqueda);
    console.log("RESULTADO::::: " + JSON.stringify(sear))
}

function enviaraPolymer(client, mensaje){
    console.log("Enviando mensaje a clientes: " + wss.clients)

    wss.clients.forEach(function each(client) {
        if (client.readyState === WebSocket.OPEN) {
          client.send(mensaje);
        }
    });
}

wss.on('close', function close(code, reason) {
    console.log('disconnected');
  });

  server.listen(webSocketPort);

  if(server.listening){
    console.log("Escuhando websokcet en puerto: " + webSocketPort)
  }