#!/bin/sh


# ./deleteAllDocuments.sh

####################################################################
# Script para importar base de datos para servicios.
# Utilizar para importar datos desde una fuente conocida.
# Cambiar variables dentro del bloque indicado
####################################################################

## Base de datos destino
#Base de datos donde se guarda la informacion
####################################################################
###### Variables a cambiar en caso de ser necesario inicio
####################################################################
toDbHost='localhost'
toDbPort='27017'
####################################################################
###### Variables a cambiar en caso de ser necesario fin
####################################################################

#No modificar el nombre de la DB
toDbName='services'
## Otras variables
#Directorio temporal donde quedaran los archivos para hacer el traspaso de datos
outDir='./colls'

#Guardando los datos en la db de destino
echo Comenzando la importacion de datos hacia: $toDbHost:$toDbPort/$toDbName
echo servicios
$MONGO_HOME/bin/mongoimport --host $toDbHost --port $toDbPort --db $toDbName --collection servicios --file $outDir/services.json
echo campos
$MONGO_HOME/bin/mongoimport --host $toDbHost --port $toDbPort --db $toDbName --collection camposservicios --file $outDir/camposservicios.json
echo grupos
$MONGO_HOME/bin/mongoimport --host $toDbHost --port $toDbPort --db $toDbName --collection gruposervicios --file $outDir/gruposervicios.json
echo ambientes
$MONGO_HOME/bin/mongoimport --host $toDbHost --port $toDbPort --db $toDbName --collection ambientes --file $outDir/ambientes.json
echo wraper fields
$MONGO_HOME/bin/mongoimport --host $toDbHost --port $toDbPort --db $toDbName --collection wraperfield --file $outDir/wraperfield.json



echo Proceso finalizado.

