#!/bin/sh

####################################################################
# Script para importar base de datos para servicios.
# Utilizar para importar datos desde una fuente conocida.
# Cambiar variables dentro del bloque indicado
####################################################################

## Base de datos destino
#Base de datos donde se guarda la informacion
####################################################################
###### Variables a cambiar en caso de ser necesario inicio
####################################################################
toDbHost='localhost'
toDbPort='27017'
####################################################################
###### Variables a cambiar en caso de ser necesario fin
####################################################################

#No modificar el nombre de la DB
toDbName='services'
## Otras variables
#Directorio temporal donde quedaran los archivos para hacer el traspaso de datos
outDir='.'

#Guardando los datos en la db de destino
echo Comenzando la modificacion de host fuse a: $toDbHost:$toDbPort/$toDbName
$MONGO_HOME/bin/mongoimport --host $toDbHost --port $toDbPort --db $toDbName --collection servicios --file $outDir/setHost.js


echo Proceso finalizado.

