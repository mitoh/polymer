'use strict';
var mongoose                = require('mongoose'),
    CamposServiciosModel    = mongoose.model('camposServicios');

exports.obtener = function(req, res){
    try{
    var objectsIds = req.params.ids.split(",").map(id => mongoose.Types.ObjectId(id));
    log("Obteniendo campos : " + objectsIds);
    }catch(err){
            res.status(401).json({message: "No se puede leer el listado de ids."})
    }

    CamposServiciosModel.find({
        '_id': {$in: objectsIds}
    }).exec(function(err, camposServicios){
        if(err){
            res.status(500).send(err)
        }

        res.status(200).send(camposServicios)
    })


    // CamposServiciosModel.find(
    //     {'_id': {$in: objectsIds}}, function(err, camposServiciosModel) {
    //         if (err) res.send(err);
    //         res.send(camposServiciosModel);
    //     });
}


exports.obtenerTodos = function(req, res){
    log("Obteniendo todos los campos");

    CamposServiciosModel.find({}, function(err, camposServiciosModel) {
            if (err) res.send(err);
            res.send(camposServiciosModel);
    });
}

var log = function(text){
    if(process.env.NODE_ENV !== "production")
    {
        console.log(text);
    }
}