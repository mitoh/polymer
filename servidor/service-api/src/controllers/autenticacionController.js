'use strict';

var mongoose = require('mongoose'),
    http = require('http'),
    Client = require('node-rest-client').Client,
    client = new Client(),
    LoginModel      = mongoose.model('login'),
    authURL = "https://arq-int.sigfe.gob.cl/cxf/autenticacion/",
    jwt = require('jsonwebtoken');

/**
 * Pregunta si el usuario esta autentiado en el sistema
 * @param {*} req 
 * @param {*} res 
 */
exports.isLogedIn = function(req, res){
    var token = req.params.token
    if(!token){
        res.status(428).send({estado:'NOK', message: "No se ha enviado un token valido"});
        return;
    }

    var tokenDecoded = {}
    
    try{
        tokenDecoded = jwt.decode(token)
    }catch(err){
        console.error("Imposible parsear token con la informacion del front end. Dando por finalizada la sesion: " + err)
    }
    var minutosPorSession = 0

    try{
        minutosPorSession = (tokenDecoded.exp - tokenDecoded.iat) / 60
    }catch(err){
        console.error("Imposible parsear token con la informacion del front end. Dando por finalizada la sesion: " + err)
    }

    var query = {
        token: token,
        isLogedIn: true
    }

    LoginModel.find(query, function(err, respuesta){
        if(err) res.send(err);

        var isLogegId = respuesta.length > 0
        var isTimeout = true;
        
        if(isLogegId){
            var loginModel = respuesta[0],
                horaLoginModel = loginModel.hora.getTime(),
                minutosConectado = (new Date().getTime() - horaLoginModel) / 60000

            if(minutosPorSession > 0){
                if(minutosConectado > minutosPorSession){
                    res.status(200).send({isLogedIn: false})
                }else{
                    res.status(200).send({isLogedIn: isLogegId})
                }
            }else{
                res.status(200).send({isLogedIn: false})
            }
        }else{
            res.status(200).send({isLogedIn: isLogegId})
        }
        
    });
}


/**
 * Inicia flujo para borrar sesion de base de datos
 * @param {*} req 
 * @param {*} res 
 */
exports.logout = function(req, res){
    
    if(!req.body.token){
        res.status(428).send({estado:'NOK', message: "No se ha enviado un token valido"});
        return;
    }

    var query = {token: req.body.token}
    LoginModel.update(query, {isLogedIn : false}, null, function(err, respuesta){
        if(err) res.send(err);
        res.status(200).send({estado:'ok'})
    });
}

/**
 * Guarda el login en la db
 * @param {*} loginModel 
 */
function doLogin(res, login){
    var loginModel = new LoginModel(login);

    loginModel.save(function(err,loginModel){
        if(err) res.send(err);
    });

}

/**
 * Autentica sobre servicio de fuse. 
 * @param {*} req 
 * @param {*} res 
 */
exports.autenticar = function(req, res){

    var usuario = req.body.usuario,
        password = req.body.password

    if(!usuario || !password){
        res.status(428).send({message: "Credenciales inválidas"});
        return;
    }

    // var url = req.body.url;
    var url = authURL;
    var body =  {
            usuario: usuario,
            password: password
        };

    var args = {
        data: JSON.stringify(body),
        headers: {
            authorization: "Basic " + Buffer.from(usuario + ":" + password).toString('base64'),
            devuelveRoles: true
        }
    };

    try{        
        var respuestaFuse = client.post(url, args, function (data, response) {
            var statusCode = response.statusCode
            log("Esta es la data del response: " + JSON.stringify(data) + "\n" + "Status code devuelto: " + statusCode);

            var login = {
                username: body.usuario, 
                token: data.token, 
                isLogedIn: true, 
                hora: new Date(), 
                metaData: "algo"}

            var out = Object;
            
            if(statusCode == 404){
                out = {
                    estado: "NOK",
                    message: "Servicio de autenticacion no encontrado"
                }
            }else if(statusCode == 401){
                out = {
                    estado: "NOK",
                    message: "Credenciales inválidas."
                }
            }else if(statusCode == 200){
                doLogin(res, login)
                out = {
                    estado: data.estado,
                    token: data.token,
                    roles: data.roles
                }
            }else{
                log("Erro en autenticacion: " + statusCode);
                out = {
                    estado: "NOK",
                    message: "Error interno del servidor. El servidor de autenticacion a respondido con codigo: " + statusCode,
                }
                statusCode = 500;
            }
                
            res.status(statusCode).send(out)
        });

        respuestaFuse.on('requestTimeout', function (req) {
            console.error('request has expired');
            res.abort();
        });
            
        respuestaFuse.on('responseTimeout', function (res) {
            console.error('response has expired');
            res.status(401).send("")
        });
        respuestaFuse.on('error', function (err) {
            console.error('Error tratando de autentica. Servidor retorna el error: ', err);
            console.error('Enviando error 401 hacia el cliente');
            res.status(401).send("")
        });
    }catch(err){
        res.status(500);            
        res.json({message: "error", desc: err});
    }
}

function log(text){
    if(process.env.NODE_ENV !== "production")
    {
        console.log(text);
    }
}