'use strict';

var options = {
    mimetypes: {
            xml: [
                "text/xml", 
                "application/xml", 
                "application/xml;charset=utf-8", 
                "application/xml; charset=utf-8", 
                "text/xml;charset=utf-8", 
                "text/xml; charset=utf-8", 
                "*/*"]		
        }
};


var https    = require('https'),
    Client  = require('node-rest-client').Client,
    client  = new Client(options);
   

/**
 * Arma request segun parametro de entrada e invoca al servicio.
 * @param {*} req 
 * @param {*} res 
 */
exports.
callService = function(req, res){
    log("Haciendo call service a  :" + req.params.callType);

    switch(req.params.callType){
        case constants.call.fuse:
            callFuseService(req, res)
        case constants.call.camunda:
            callCamundaService(req, res)
        default: //Lanzar error. Not supported
    }        
}


/**
 * Llama a servicio fuse
 * @param {*} req 
 * @param {*} res 
 */
function callFuseService(req, res){
    log("Entrando a llamar a servicio en fuse")

    var options = getOptions(req, res)

    switch(options.method){
        case "POST":
            log("Metodo reconocido: POST")
            handlePOST(options, res)
            break;
        case "GET":
            log("Metodo reconocido: GET")
            handleGET(options, res)
            break;
        case "PUT":
            res.send({"message":"No implementado aun."})
            break;
        case "DELETE":
            res.send({"message":"No implementado aun."})
            break;
        default:
            res.send({"message":"Method not found!"})
    }
}

function handleGET(options, res){
    var completeURL = options.host+":"+options.port+options.path,
        computedBody;
    options.completeURL = completeURL;

    log("Hacia servicio: " + options.completeURL)

    if(options.logBody){
        log("Body: " + JSON.stringify(options.body))
    }

    if(options.typeJson){
        computedBody = JSON.stringify(options.body);
    }else{
        computedBody = options.body;
    }

    var args = {
        data:  computedBody,
        headers: options.headers
    };

    try{
        /**
         * TODO: Solucionar. Cuando el content-type es xml, cliente parsea de igunal forma a json.
         * Se toma la desicion de limpiar todos los parsers para entregar una respuesta sin 
         * ningun manejo, la cual se convierte a String antes de su salida.
         * */
        if( args.headers["content-type"] == "text/xml" || 
            args.headers["content-type"] == "application/xml"){
            client.parsers.clean();	
         }

        var req =   client.get(options.completeURL, args, function (data, response) {

            var statusCode = response.statusCode
            out
            if(response.headers["content-type"] == "text/xml" || 
                response.headers["content-type"] == "application/xml"){
                    var dataString = data.toString().replace(/[\n\r\t]/g, "");
                log("Esta es la data del response: " + JSON.stringify(dataString.substring(0, 100) + " (...)") + "\n"
                + "Status code devuelto: " + statusCode);

                var out = {
                    data: dataString
                }

                if(dataString != null && dataString.length > 10){
                    out.estado = "OK"
                }

                res.status(statusCode).send(out)
            }else{
                log("Esta es la data del response: " + JSON.stringify(data) + "\n"
                + "Status code devuelto: " + statusCode);
                res.status(statusCode).send(data)
            }
        });
        req.on('requestTimeout', function (req) {
            console.error('request has expired');
            req.abort();
        });
            
        req.on('responseTimeout', function (res) {
            console.error('response has expired');
            res.status(503).send("")
        });
        req.on('error', function (err) {
            console.error('request error', err);
            res.status(503).send("")
        });
    }catch(ex){
        console.error("Ha ocurrido un error: " + ex)
        res.status(503).send({err: ex})
    }
}

function handlePOST(options, res){
    var post_options = {
        host: 'closure-compiler.appspot.com',
        path: '/compile',
        method: 'POST',
        headers: {
            'Content-Type': 'application/x-www-form-urlencoded',
            'Content-Length': Buffer.byteLength(post_data)
        }
    };

        // Set up the request
    var post_req = https.request(post_options, function(res) {
        res.setEncoding('utf8');
        res.on('data', function (chunk) {
            console.log('Response: ' + chunk);
        });
    });

    // post the data
    post_req.write(post_data);
    post_req.end();

}

/**
 * Obtiene objeto para armar mensaje de llamada a fuse
 * @param {*} req 
 * @param {*} res 
 */
function getOptions(req, res){
    var builtedPath = req.body.path,
        computedHeaders,
        typeJson;

    if(req.body.urlParams){
        builtedPath = builtedPath + req.body.urlParams
    }

    if(req.body.headers){
        computedHeaders = req.body.headers
    }else{
        /**
         * Si no lleva headers, se setean los
         * que vienen por defecto en la llamada
         */
        computedHeaders = {
            "content-type": req.headers["content-type"]
        }
    }

    typeJson = computedHeaders["content-type"] == "application/json";

    var options = {
        host: req.body.host,
        port: req.body.port,
        path: builtedPath,
        method: req.body.method,
        headers: computedHeaders,
        body: req.body.body,
        logBody: req.body.logBody,
        typeJson: typeJson
    }

    return options;
}

function callCamundaService(req, res){
}

var constants = {
    call:{
        fuse:       "fuse",
        camunda:    "camunda"
    },
    protocol:{
        http: "http"
    }
}

function log(text){
    if(process.env.NODE_ENV !== "production")
    {
        console.log(text);
    }
}