'use strict';
const   http    = require('http'),
        https    = require('https'),
        Client  = require('node-rest-client').Client,
        client  = new Client();
   

/**
 * Arma request segun parametro de entrada e invoca al servicio.
 * @param {*} req 
 * @param {*} res 
 */
exports.callService = function(req, res){
    log("Haciendo call service a  :" + req.params.callType);

    switch(req.params.callType){
        case constants.call.fuse:
            callFuseService(req, res)
        case constants.call.camunda:
            callCamundaService(req, res)
        default: //Lanzar error. Not supported
    }        
}

/**
 * Llama a servicio fuse
 * @param {*} req 
 * @param {*} res 
 */
function callFuseService(req, res){
    log("Entrando a llamar a servicio en fuse")

    var options = getOptions(req, res)

    switch(options.method){
        case "POST":
            log("Metodo reconocido: POST")
            handlePOST(options, res)
            break;
        case "GET":
            log("Metodo reconocido: GET")
            handleGET(options, res)
            break;
        case "PUT":
            res.send({"message":"No implementado aun."})
            break;
        case "DELETE":
            res.send({"message":"No implementado aun."})
            break;
        default:
            res.send({"message":"Method not found!"})
    }
}

function handleGET(options, res){
    var completeURL = options.protocolo+"://"+options.host+":"+options.port+options.path,
        computedBody;
    options.completeURL = completeURL;

    log("Hacia servicio: " + options.completeURL)

    if(options.logBody){
        log("Body: " + JSON.stringify(options.body))
    }

    if(options.typeJson){
        computedBody = JSON.stringify(options.body);
    }else{
        computedBody = options.body;
    }

    var args = {
        data:  computedBody,
        headers: options.headers
    };

    try{
        /**
         * TODO: Solucionar. Cuando el content-type es xml, cliente parsea de igunal forma a json.
         * Se toma la desicion de limpiar todos los parsers para entregar una respuesta sin 
         * ningun manejo, la cual se convierte a String antes de su salida.
         * */
        if( args.headers["content-type"] == "text/xml" || 
            args.headers["content-type"] == "application/xml"){
            client.parsers.clean();	
         }

        var req =   client.get(options.completeURL, args, function (data, response) {

            var statusCode = response.statusCode
            out
            if(response.headers["content-type"] == "text/xml" || 
                response.headers["content-type"] == "application/xml"){
                    var dataString = data.toString().replace(/[\n\r\t]/g, "");
                log("Esta es la data del response: " + JSON.stringify(dataString.substring(0, 100) + " (...)") + "\n"
                + "Status code devuelto: " + statusCode);

                var out = {
                    data: dataString
                }

                if(dataString != null && dataString.length > 10){
                    out.estado = "OK"
                }

                res.status(statusCode).send(out)
            }else{
                log("Esta es la data del response: " + JSON.stringify(data) + "\n"
                + "Status code devuelto: " + statusCode);
                res.status(statusCode).send(data)
            }
        });
        req.on('requestTimeout', function (req) {
            console.error('request has expired');
            req.abort();
        });
            
        req.on('responseTimeout', function (res) {
            console.error('response has expired');
            res.status(503).send("")
        });
        req.on('error', function (err) {
            console.error('request error', err);
            res.status(503).send("")
        });
    }catch(ex){
        console.error("Ha ocurrido un error: " + ex)
        res.status(503).send({err: ex})
    }
}

/**
 * Maneja post
 * @param {*} options 
 * @param {*} res 
 */
function handlePOST(options, res){
    var computedBody,
        protocolo = options.protocolo;

    if(options.logBody){
        //log("Body: " + JSON.stringify(options.body))
    }

    if(options.typeJson){
        computedBody = JSON.stringify(options.body);
    }else{
        computedBody = options.body;
    }

    const slash = "/"
    if(!options.path.startsWith(slash)){
        options.path = slash + options.path 
    }

    var args = {
        path:  options.path,
        host: options.host,
        port: options.port,
        method: options.method,
        headers: {
            "content-type": options.headers["content-type"]
        }
    };

    //Si existe autorizacion
    if(options.headers && options.headers.authorization){
        args.headers["authorization"] = options.headers.authorization
    }

    log("Argumentos: " + JSON.stringify(args))

    if(protocolo == "https"){
        try{
            log("Llamando via https")
            initCall(https, args, computedBody, res)
        }catch(ex){
            deliverError(res, ex)
        }
    }else if(protocolo == "http"){
        try{
            log("Llamando via http")
            initCall(http, args, computedBody, res)
        }catch(ex){
            deliverError(res, ex)
        }
    }
 }

/**
 * Entrega el error al solicitante
 * @param {*} res 
 * @param {*} err 
 */
 function deliverError(res, err){
     var errOut = {
        message: "Hubo un error indeterminado en el sistema. Favor contacte con el administrador",
        err: err
     }
    res.status(500).send(errOut)
 }

 /**
  * Afectua la llamada y contiene la logica para mostrarla
  */
 function initCall(clientCall, args, body, res){
    log("Haciendo llamada")
    const postReq = clientCall.request(args, (resp) => {
        //resp.setEncoding('utf8');
        const statusCode = resp.statusCode

        if(statusCode == 401){
            res.status(statusCode).send()
            return;
        }
        var out = '';

        resp.on('data', (chunk) => {
            console.log('Response: ' + chunk.toString());
            if(chunk){
                out += chunk;
            }
        });

        resp.on("error", function(err){
            console.log("Hay un error");
        });

        resp.on("end", function(){
            try{
                out = JSON.parse(out)
                if(Array.isArray(out)){
                    if(out.length == 1){
                        //out = out[0]
                    }
                }
            }catch(parseError){
                const errParse = {
                    message: "No se puede parsear respuesta a Json, enviando respuesta en crudo",
                    err: parseError
                }
                log(JSON.stringify(errParse))
            }
            
            console.log(JSON.stringify({
                message: "Terminando la respuesta",
                out: out,
                stausCode : statusCode
            }));
            
            res.status(resp.statusCode).send(out)
        });
    });
    postReq.on('error', (err) => {
        const outErr = {
            message: "Error en el servidor de servicios",
            error: err
        }
        console.log('Error en el servidor. Entregando mensaje: : ' + JSON.stringify(outErr));
        res.status(500).json(outErr)
    })
    postReq.on('abort', (err) => {
        const errAbort = {
            message: "Llamada abortada por el servidor. Pongase en contacto con el administrador",
            err: err
        }
        console.log("Llamada abortada. Entragando mensaje: " + JSON.stringify(errAbort));
        res.status(500).send(errAbort)
    })

    log("post the data: " + body)
    postReq.write(body);
    log("end")
    postReq.end();
 }

/**
 * Obtiene objeto para armar mensaje de llamada a fuse
 * @param {*} req 
 * @param {*} res 
 */
function getOptions(req, res){
    var builtedPath = req.body.path,
        computedHeaders,
        typeJson;

    if(req.body.urlParams){
        builtedPath = builtedPath + req.body.urlParams
    }

    if(req.body.headers){
        computedHeaders = req.body.headers
    }else{
        /**
         * Si no lleva headers, se setean los
         * que vienen por defecto en la llamada
         */
        computedHeaders = {
            "content-type": req.headers["content-type"]
        }
    }

    typeJson = computedHeaders["content-type"] == "application/json";

    var options = {
        host: req.body.host,
        port: req.body.port,
        path: builtedPath,
        method: req.body.method,
        headers: computedHeaders,
        body: req.body.body,
        logBody: req.body.logBody,
        typeJson: typeJson,
        protocolo: req.body.protocolo
    }

    return options;
}

function callCamundaService(req, res){
}

var constants = {
    call:{
        fuse:       "fuse",
        camunda:    "camunda"
    },
    protocol:{
        http: "http"
    }
}

function log(text){
    if(process.env.NODE_ENV !== "production")
    {
        console.log(text);
    }
}

function error(text){
    if(process.env.NODE_ENV !== "production")
    {
        console.error(text);
    }
}