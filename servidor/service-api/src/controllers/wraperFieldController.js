'use strict';

var mongoose = require('mongoose'),
    WraperFieldModel      = mongoose.model('wraperField');

/**
 * Guarda un wraperField
 * @param {*} req 
 * @param {*} res 
 */

exports.guardarWraperField = function(req, res){
    var body = req.body;
    log("Guardando wraperField: " + JSON.stringify(body))

    if(!body){
        res.status(412).send({message: "No body"})
    }

    var wraperField = new WraperFieldModel(body);

    wraperField.save(function(err, wraperField) {
        if (err)
            res.status(500).send(err);
        
        log("wraperField guardado")
        res.status(201).json(wraperField);
    });
}

/**
 * Busca wraperField segun parametro
 * @param {*} req 
 * @param {*} res 
 */
exports.buscarWraperField = function(req, res){
    var body = req.body;

    WraperFieldModel.find(body, function(err, wraperField) {
        if (err)
            res.status(500).send(err);
        
        log("wraperField encontrado: " + JSON.stringify(wraperField))
        res.status(200).json(wraperField);
    });
}

var log = function(text){
    if(process.env.NODE_ENV !== "production")
    {
        console.log(text);
    }
}