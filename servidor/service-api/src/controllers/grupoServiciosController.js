var mongoose = require('mongoose'),
    GrupoServiciosModel = mongoose.model('grupoServicios'),
    ServiciosModel = mongoose.model('servicios'),
    WraperFieldModel = mongoose.model('wraperField');

exports.guardar = function(req, res){
    log("Guardando: grupos de servicios con: " + JSON.stringify(req.body))

    var title = req.body.title
    
    if(!title){
        res.status(412).send({message: "Nombre existente"})
        return;
    }

    var query = {
        title: req.body.title
    }

    GrupoServiciosModel.find(query, function(err, grupoEncontrado){
        if(err){
            res.status(500).send({message: "No se puede encontrar los grupos", err: err})
            return;
        }else{
            if(grupoEncontrado && grupoEncontrado.length == 0){
                var grupoServiciosModel = new GrupoServiciosModel(req.body);

                grupoServiciosModel.save(function(err, grupoServiciosModel) {
                    if (err)
                        res.send(err);
                    res.status(201);            
                    res.json(grupoServiciosModel);
                });
            }else{
                res.status(412).send({message: "El nombre del grupo ya existe"})
                return;
            }
        }
    })

    
}

exports.obtener = function(req, res){
    log("Obteniendo uno con: " + req.params.grupoServiciosKey)
    GrupoServiciosModel.findById(req.params.grupoServiciosKey, function(err, grupoServiciosModel) {
        if (err)
            res.send(err);
        res.json(grupoServiciosModel);
    });
}
exports.modificar = function(req, res){
    GrupoServiciosModel.findOneAndUpdate({_id: req.params.grupoServiciosKey}, req.body, {new: true}, function(err, grupoServiciosModel) {
        if (err)
            res.status(400).send(err);
        res.json(grupoServiciosModel);
    });
}
exports.asignarServiciosAGrupo = function(req, res){
    log("Entrando a asignar servicios a grupos")
    var serviciosAgregar = req.body.agregarServicios,
        serviciosEliminar = req.body.eliminarServicios,
        key = req.params.grupoServiciosKey,
        grupo = req.body

    for(var i=0;i<serviciosAgregar.length;i++){
        var servicioAgregar = serviciosAgregar[i],
            exist = grupo.servicios.find(servicio => {
                return servicio == servicioAgregar
            })

        if(!exist){
            grupo.servicios.push(servicioAgregar)
        }
    }

    for(var i=0;i<serviciosEliminar;i++){
        var servicioEliminar = serviciosEliminar[i];
        grupo.servicios.remove(servicioEliminar)
    }  
    
    grupo.agregarServicios = null;
    grupo.eliminarServicios = null;

    log("Quitando servicios seleccionados[" + serviciosAgregar + "] de los demás grupos");
    GrupoServiciosModel.update({servicios: {$all: serviciosAgregar}}, {$pullAll: {servicios: serviciosAgregar}}, function(err){
        if (err){
            res.status(400)
            res.send(err);
        }

        log("Guardando grupo seleccionado: " + grupo.title)
        GrupoServiciosModel.findOneAndUpdate({_id: key}, grupo, {new: true},function(err, grupoServiciosModel) {
            if (err){
                res.status(400)
                res.send(err);
            }
    
            log("Guardando servicios con el nuevo grupo asignado: " + serviciosAgregar)
            ServiciosModel.update({_id: {$in: serviciosAgregar}}, {$set: {"grupoServicio" : grupoServiciosModel._id.toString()}}, function(err, algo){
                if (err){
                    res.status(400)
                    res.send(err);
                }
    
                res.status(200).send({"message":"ok"});
            })
        })
    })
}
exports.borrar = function(req, res){
    log("Intentando borrar con: " + req.params.grupoServiciosKey)    
    GrupoServiciosModel.remove({
        _id: req.params.grupoServiciosKey
    }, function(err, grupoServiciosModel) {
        if (err)
            res.send(err);
        res.json({ message: 'El Grupo fue eliminado correctamenete ' });
    });
}

/**
 * Retorna todos los grupos
 * 
 * query: req.headers.username
 * @param {*} req 
 * @param {*} res 
 */
exports.obtenerTodos = function(req, res){

    var username = req.headers.username,
        query = {}

    if(username){
        query = {
            username: username
        }
        log("Usando query para obtener los grupos: " + JSON.stringify(query))
    }

    GrupoServiciosModel.find(query, function(err, grupoServiciosModel) {
        // log("Tratando de buscar todos: " + grupoServiciosModel)
        if (err) res.json(err);
        log("Obteniendo todos los grupos de servicios.")
        res.json(grupoServiciosModel);
    });
};

/**
 * Retorna solamente los grupos que tienen al menos un servicio dentro
 */
exports.obtenerGruposYServicios = function(req, res){
    log("Obteniendo grupos de servicios y servicios con roles: " + JSON.stringify(req.body))

    var roles = req.body;

    if(!roles){
        res.status(428).send({estadp: 'NOK', message: "No se encuentran los roles del usuario"})
        return;
    }

    GrupoServiciosModel.find({
        "servicios.0": {
            "$exists": true
        }
    }).populate({
                path: 'servicios',
                match: {roles: {$in: roles}},
                populate: {
                    path: 'campos.wraperField'
                }
            })
    .exec(function(err, grupoServiciosModel){
        res.send(grupoServiciosModel.filter(grupo => {
            return grupo.servicios.length > 0
        }))
    })
}


var log = function(text){
    if(process.env.NODE_ENV !== "production")
    {
        console.log(text);
    }
}