'use strict';

    var https    = require('https'),
        http    = require('http'),
    Client  = require('node-rest-client').Client,
    client  = new Client();
   

exports.mockService1 = function(req, res){
    var url= "http://" + req.body.host + ":" + req.body.port + req.body.path;
    log("URL: " + url)

    var args = {
        data:  req.body,
        headers: req.headers
    };

    try{
        var req =   client.post(url, args, function (data, response) {
        var statusCode = response.statusCode
                log("Esta es la data del response: " + JSON.stringify(data) + "\n"
            + "Staus code devuelto: " + statusCode);
                res.status(statusCode).send(data)
        });

        req.on('requestTimeout', function (req) {
            console.error('request has expired');
            req.abort();
        });

        req.on('responseTimeout', function (res) {
        console.error('response has expired');
        res.status(503).send("")
        });
        req.on('error', function (err) {
            console.error('request error', err);

            var out = {
                stack: err.stack,
                message: err.message,
                code: err.code
            }
            res.status(503).send(out)
        });
    }catch(ex){
        console.error("Servicio no disponible!")
        res.status(503).send({message: "Servicio no disponible"})
    }
}

exports.mockService2 = function(req, res){
    var postData = JSON.stringify(req.body)

    var args = {
        path:  req.body.path,
        host: req.body.host,
        port: req.body.port,
        method: req.body.method,
        headers: {
            "Content-Type": "application/json"
        }
    };

    log("postData: " + JSON.stringify(postData))
    log("ARGS: " + JSON.stringify(args))

    try{
        // Set up the request
        log("Haciendo llamada")
        var postReq = http.request(args, function(resp) {
            resp.setEncoding('utf8');
            resp.on('data', function (chunk) {
                console.log('Response: ' + chunk);
                res.status(200).send(JSON.parse(chunk))
            });
        });

        log("post the data")
        postReq.write(postData);
        log("end")
        postReq.end();
    }catch(ex){
        console.error("Servicio no disponible!")
        res.status(503).send({message: "Servicio no disponible"})
    }
}


function log(text){
    if(process.env.NODE_ENV !== "production")
    {
        console.log(text);
    }
}