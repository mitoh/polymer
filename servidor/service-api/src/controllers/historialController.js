'use strict';

var mongoose = require('mongoose');
var mongoosePaginate = require('mongoose-paginate');

exports.buscar = function(req, res){
   res.json({message: "Not implemented yet!"})
}



/**
 * Inserta el ticket en el historial 
 * @param {*} req 
 * @param {*} res 
 */
exports.insertarTicket = function(req, res){
    try{

        var idHistorial = req.params.idHistorial,
            ticket = req.body
        var Model = obtenerModel();

        log("Agregar ticket a historial con id: " + idHistorial)    

        Model.update({_id: idHistorial}, {$set: {ticket: ticket}}, function(err, ticket) {
            if (err)
                res.send(err);

            res.status(200);            
            res.json(ticket);
        });

    }catch(err){
        res.status(500);            
        res.json({message: "error", desc: err});
    }
}


/**
 * Inserta la respuesta en el historial 
 * @param {*} req 
 * @param {*} res 
 */
exports.guardarRespuesta = function(req, res){
    try{

        var respuetaStr = JSON.stringify(req.body).replace(/\$/g, "")
        try{
            //Trata de convertir la respuesta en objeto json
            respuetaStr = JSON.parse(respuetaStr)
        }catch(err){
            log("No se puede convertir la respueta en objeto, guardando como String")
            log(err)
        }

        var idHistoria = req.params.idHistoria,
            respuesta = respuetaStr
        var Model = obtenerModel();

        log("Agregando respueta a historial con id: " + idHistoria)    

        Model.update({_id: idHistoria}, {$set: {"respuesta": respuesta}}, function(err, respuestaGuardar) {
            if (err){
                res.status(500).send(err);
                return;
            }

            res.status(200);
            res.json(respuestaGuardar);
        });

    }catch(err){
        res.status(500);            
        res.json({message: "error", desc: err});
    }
}


/**
 * Obtiene el historial de llamadas.
 * @param {*} req 
 * @param {*} res 
 */
exports.obtener = function(req, res){
    try{
        var username = req.headers.username;
        var Model = obtenerModel();

        var elementosPorPagina = 10,
            numeroPagina = 1
        
            
        try{
            elementosPorPagina = parseInt(req.headers.elementosporpagina)
            numeroPagina = parseInt(req.headers.numeropagina)
        }catch(ignored){}     
        
        var query = {usuario: username},
        options = {page: numeroPagina, limit: elementosPorPagina, sort: {fecha:'desc'}};

        log("Buscando historial con: " + JSON.stringify({query: username, options: options}))
        
        Model.paginate(query, options, function(err, historial){

            if(err){
                log("Error recuperando historial: " + err)
                res.send(err);
                return ;
            }

            res.status(200).json(historial);
        });
    }catch(err){
        res.status(500);            
        res.json({message: "error", desc: err});
    }
}

/**
 * Metodo provisorio.
 * Guarda en una coleccion los eventos
 * @param {*} req 
 * @param {*} res 
 */
exports.guardar = function(req, res){
    try{
        //var nombreCollection = req.params.nombre,
        var nombreCollection = "llamadaServicios",
            Model = obtenerModel();
       
        var auditoria = new Model({
            usuario: req.body.usuario,
            data: req.body.data,
            servicio: req.body.servicio,
            fecha: Date.now(),
            roles: req.body.roles
        })

        auditoria.save(function(err, serviciosModel) {
            if (err)
                res.send(err);
            res.status(201);            
            res.json(serviciosModel);
        });

    }catch(err){
        res.status(500);            
        res.json({message: "error", desc: err});
    }
}


/**
 * Crea el modelo en la APP
 */
var obtenerModel = function(){
    var nombreCollection = "llamadaServicios",
        Model;

    try{
        Model = mongoose.model(nombreCollection);
    }catch(err){
        if(!Model){
            var schema = new mongoose.Schema({
                usuario: String,
                fecha: {
                    type: Date
                },
                data: {
                    type: Object
                },
                servicio:{
                    type: Object
                },
                respuesta: Object,
                ticket: Object
            })

            schema.plugin(mongoosePaginate)

            Model = mongoose.model(nombreCollection, schema);
        }
    }

    return Model
}



var log = function(text){
    if(process.env.NODE_ENV !== "production")
    {
        console.log(text);
    }
}