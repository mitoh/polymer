'use strict';

var mongoose            = require('mongoose'),
    ServiciosModel      = mongoose.model('servicios'),
    CampoServicioModel  = mongoose.model('camposServicios');

exports.guardar = function(req, res){
    log("Guardando: grupos de servicios con: " + JSON.stringify(req.body))
    var serviciosModel = new ServiciosModel(req.body);

    serviciosModel.save(function(err, serviciosModel) {
        if (err)
            res.send(err);
        res.status(201);            
        res.json(serviciosModel);
    });
}

exports.obtenerPorGrupo = function(req, res){
    log("Obteniendo uno con: " + req.params.serviciosKey)
    ServiciosModel.find({'grupoServicios' : req.params.serviciosKey}, function(err, serviciosModel) {
        if (err) res.send(err);

        CampoServicioModel.find({'servicioKey' : serviciosModel.key}, function(err, camposServicios){
            if (err) res.send(err);

            serviciosModel.camposServicios = camposServicios
        });

        res.json(serviciosModel);
    });
}

exports.obtener = function(req, res){
    log("Obteniendo uno con: " + req.params.serviciosKey)
    ServiciosModel.findById(req.params.serviciosKey, function(err, serviciosModel) {
        if (err)
            res.send(err);
        res.json(serviciosModel);
    });
}

exports.modificar = function(req, res){
    ServiciosModel.findOneAndUpdate({_id: req.params.serviciosKey}, req.body, {new: true}, function(err, serviciosModel) {
        if (err)
            res.send(err);
        res.json(serviciosModel);
    });
}

exports.borrar = function(req, res){
    ServiciosModel.remove({
        _id: req.params.serviciosKey
    }, function(err, serviciosModel) {
        if (err)
            res.send(err);
        res.json({ message: 'El servicio fue eliminado correctamenete ' });
    });
}

exports.obtenerTodosSinGrupo = function(req, res){
    ServiciosModel.find({}, function(err, serviciosModel) {
        //log("Tratando de buscar: " + serviciosModel)
        if (err)
            res.send(err);
       // log("Obteniendo todos los grupos de servicios con: " + JSON.stringify(req.body))
        res.json(serviciosModel);
    });
};

var log = function(text){
    if(process.env.NODE_ENV !== "production")
    {
        console.log(text);
    }
}