'use strict';

var mongoose = require('mongoose'),
    AmbienteModel = mongoose.model('ambiente');

/**
 * Guarda un nuevo ambiente
 * @param {*} req 
 * @param {*} res 
 */
exports.guardarAmbiente = (req, res) => {
    log("Guardando ambiente nuevo con: " + JSON.stringify(req.body))
    var ambienteModel = new AmbienteModel(req.body);

    ambienteModel.save(function (err, ambienteGuardado) {
        if (err) {
            res.send(err);
            return;
        }
        res.status(200).send(ambienteGuardado);
    });
}

/**
 * Modifica un ambiente
 * @param {*} req 
 * @param {*} res 
 */
exports.updateAmbiente = (req, res) => {
    try {
        var ambiente = req.body
        log("Updateando ambiente: " + JSON.stringify(ambiente))

        AmbienteModel.findOneAndUpdate({ _id: ambiente._id }, ambiente, { new: true }, function (err, respuestaGuardar) {
            if (err) {
                res.status(500).send(err);
                return;
            }

            res.status(200).send(respuestaGuardar);
        });

    } catch (err) {
        res.status(500);
        res.json({ message: "error", desc: err });
    }
}

/**
 * Elimina ambiente por parametro
 * @param {*} req 
 * @param {*} res 
 */
exports.elimnarAmbiente = function (req, res) {
    var idEliminar = req.body._id
    log("Intentando borrar ambiente con: " + idEliminar)

    AmbienteModel.remove({_id: idEliminar}, function (err, grupoServiciosModel) {
        log("Respuesta del eliminar: " + JSON.stringify(grupoServiciosModel))
        if (err){
            res.send(err);
            return;
        }
        res.status(200).send({message: 'El Grupo fue eliminado correctamenete'});
    });
}

/**
 * Obtiene todos los ambientes segun los roles
 * por parametro 
 * @param {*} req 
 * @param {*} res 
 */
exports.obtenerTodos = function (req, res) {
    var roles = req.body.roles,
        proyeccion = req.body.proyeccion,
        username = req.body.username,
        query = {};

    if (!proyeccion) {
        proyeccion = {
            // roles: 0,
            // _id: 0,
            // usuarioCreador:0
        }
    }

    if (roles) {
        query.roles = {
            $in: roles
        }
    }

    if (username) {
        query.username = {
            username
        }
    }

    log("Obteniendo todos los ambientes con roles: " + roles)

    AmbienteModel.find(query, proyeccion, function (err, serviciosModel) {
        if (err) {
            res.status(500).send(err);
            return;
        }

        res.status(200).json(serviciosModel);
    });
}

var log = function (text) {
    if (process.env.NODE_ENV !== "production") {
        console.log(text);
    }
}