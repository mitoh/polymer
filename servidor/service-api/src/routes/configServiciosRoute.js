'use strict';
module.exports = function(app) {
    var grupoServiciosController    =   require('../controllers/grupoServiciosController'),
        serviceController           =   require('../controllers/serviciosController'),
        camposServiciosController   =   require('../controllers/camposServiciosController'),
        callController              =   require('../controllers/call/callController'),
        historialController         =   require('../controllers/historialController'),
        autenticacionController     =   require('../controllers/autenticacionController'),
        wraperFieldController     =   require('../controllers/wraperFieldController'),
        mockController     =   require('../controllers/mockController'),
        ambienteController     =   require('../controllers/ambienteController');
        
        
    /**
     * Grupo de servicios
     */
    app.route('/grupoServicios')
        .get(grupoServiciosController.obtenerTodos)
        .post(grupoServiciosController.guardar)

    app.route('/grupoServicios/id/:grupoServiciosKey')
        .get(grupoServiciosController.obtener)
        .put(grupoServiciosController.modificar)
        .delete(grupoServiciosController.borrar); 

    app.route('/grupoServicios/servicios/:grupoServiciosKey')
        .put(grupoServiciosController.asignarServiciosAGrupo);

    app.route('/grupoServicios/servicios')
        .post(grupoServiciosController.obtenerGruposYServicios)

    /**
     * Servicios
     */
    app.route('/servicios/')
        .get(serviceController.obtenerTodosSinGrupo)
        .post(serviceController.guardar);

    app.route('/servicios/:serviciosKey')
        .get(serviceController.obtener)
        .put(serviceController.modificar)
        .delete(serviceController.borrar); 

    app.route('/servicios/grupoServicios/:grupoServiciosKey')
        .get(serviceController.obtenerPorGrupo)
        .put(serviceController.modificar)
        .delete(serviceController.borrar); 

    app.route('/campos/:ids')
        .get(camposServiciosController.obtener);
    
    app.route('/campos/')
        .get(camposServiciosController.obtenerTodos)

    app.route('/historial/')
        .post(historialController.guardar)
        .get(historialController.obtener);

    app.route('/historial/ticket/:idHistorial')
        .put(historialController.insertarTicket);

    app.route('/historial/respuesta/:idHistoria')
        .put(historialController.guardarRespuesta);

    /**
     * Autenticacion 
     */
    app.route('/autenticacion')
        .post(autenticacionController.autenticar)
        .delete(autenticacionController.logout);

    app.route('/autenticacion/:token')    
        .get(autenticacionController.isLogedIn);

    /**
     * prefijos y sufijos
     */
    app.route('/wraperField')
        .post(wraperFieldController.guardarWraperField)
        .get(wraperFieldController.buscarWraperField)

    /** 
     * Ambiente
     */
    app.route('/ambientes')
        .post(ambienteController.obtenerTodos)
        .delete(ambienteController.elimnarAmbiente);

    app.route('/guardarAmbiente')
        .post(ambienteController.guardarAmbiente)
        .put(ambienteController.updateAmbiente);

    /**
     * Llamada a distintos servicios. Discrimina por parametro callType 
     * par saber como armar request
     */
     app.route('/call/:callType')
     .post(callController.callService);

     app.route('/mock1').post(mockController.mockService1)
     app.route('/mock2').post(mockController.mockService2)
};