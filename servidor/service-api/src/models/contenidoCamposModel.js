'use strict';
var mongoose = require('mongoose');
var Schema = mongoose.Schema;

var contenidoCampo = new Schema({
    key: String ,
    campoServicio: {type: Schema.Types.ObjectId, ref: 'camposServicios'},
    contenido: [
        {
            key: String,
            value: String
        }
    ],
    dropFileText: String,
    selectFileButtonText: String,
    usuarioCreador: { type: String },
    fechaCreacion: { 
        type: Date,
        default: Date.now()
    }
});
module.exports = mongoose.model('contenidoCampo', contenidoCampo);