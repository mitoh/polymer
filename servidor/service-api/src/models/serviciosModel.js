'use strict';
var mongoose = require('mongoose');
var Schema = mongoose.Schema;

var servicios = new Schema({
    title: String,
    grupoServicio:  { type: Schema.Types.ObjectId, ref: 'grupoServicios' },
    campos: [
        { 
            parameterName: String,
            parameterType: {
                type: String, 
                enum: ['body', 'url', 'asBody']
            },
            sendAs: String,
            order: Number,
            fromField: {type: Schema.Types.ObjectId, ref: 'camposServicios'},
            wraperField: {type: Schema.Types.ObjectId, ref: 'wraperField'},
            defaultValue: String,
            hidden: Boolean
        }
    ],
    autenticacion: Boolean,
    contentType: String,
    host: String,
    port: String,
    path: String,
    method: String,
    handleAs: String,
    actionTitle: String,
    usuarioCreador: String,
    fechaCreacion: { 
        type: Date,
        default: Date.now()
    },
    roles: [String],
    user:[String]
});
module.exports = mongoose.model('servicios', servicios);