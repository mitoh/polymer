'use strict';
var mongoose = require('mongoose');
var Schema = mongoose.Schema;

var grupoServicios = new Schema({
    title: String,
    desc: String,
    host: String,
    key: String,
    servicios: [{ type: Schema.Types.ObjectId, ref: 'servicios' }],
    username: String,
    fechaCreacion: String
});

module.exports = mongoose.model('grupoServicios', grupoServicios);