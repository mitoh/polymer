'use strict';
var mongoose = require('mongoose');
var Schema = mongoose.Schema;

var ambiente = new Schema({
    host: String,
    nombre:  String,
    usuarioCreador: String,
    username: String,
    fechaCreacion: Date,
    port: String,
    roles: [String],
    protocolo: String,
    fechaCreacion: Date
});
module.exports = mongoose.model('ambiente', ambiente);