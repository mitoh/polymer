'use strict';
var mongoose = require('mongoose');
var Schema = mongoose.Schema;

var login = new Schema({
    username: String,
    token:  String,
    isLogedIn: Boolean,
    hora: Date,
    metaData: Object
});
module.exports = mongoose.model('login', login);