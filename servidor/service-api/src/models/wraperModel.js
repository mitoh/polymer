'use strict';
var mongoose = require('mongoose');
var Schema = mongoose.Schema;

var wraperField = new Schema({
    //Titulo identificativo
    title: String,
    //Parte superior del envoltorio
    prefix:  String,
    //Parte inferior del nevoltorio
    suffix:  String,
    //Fecha creacion
    date: Date,
    //Si es seleccionado por defecto
    selected: Boolean,
    //Usuario creador. Sirve para filtrar
    user:String,
    /**
     * Informacion para la logica. Reemplaza las courrencias en el prefijo y suffijo
     * con el codigo javascript dentro de la propiedad execute
     */
    dinamic: [
        {
            tag: String,
            execute: String,
            ns: String
        }
    ]
}, {collection: 'wraperfield'});
module.exports = mongoose.model('wraperField', wraperField);