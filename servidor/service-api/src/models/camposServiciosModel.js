'use strict';
var mongoose = require('mongoose');
var Schema = mongoose.Schema;

var camposServicios = new Schema({
    key: { type: String },
    title: String,
    type: {
        type: String,
        enum: ['text', 'list', 'file', 'area', 'body']
    },
    content: Schema.Types.Mixed,
    usuarioCreador: { type: String },
    fechaCreacion: { 
        type: Date,
        default: Date.now()
    }
});
module.exports = mongoose.model('camposServicios', camposServicios);