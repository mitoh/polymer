var ini = require("ini"),
    fs = require("fs"),
    mongoose = require('mongoose');

var config = ini.parse(fs.readFileSync('./config/audit-config.ini', 'utf-8'));
console.log("Config" + JSON.stringify(config))

/**
 * Conexion mongo
 */
mongoose.Promise = global.Promise;
mongoose.connect('mongodb://'+ config.mongo.host + ':' + config.mongo.port + '/' + config.mongo.db, { useNewUrlParser: true });

/**
 * modelos
 */
var AuditoriaMessageModel = require("./src/model/AuditoriaMessageModel")


/**
 * Controllers
 */
var wsController = require('./src/controller/WebSocketAuditController'),
    topicController = require('./src/controller/TopicAuditController')


/**
 * Inicio
 */
wsController.setup(config);
topicController.setup(config)

wsController.levantarServidor()










