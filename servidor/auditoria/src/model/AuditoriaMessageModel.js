'use strict';
var mongoose = require('mongoose');
var Schema = mongoose.Schema;

var mensajeAuditoria = new Schema({
    topicId: String,
    wsId:  [String],
    date: Date,
    message: Object,
    // ['json 'text']
    messageType: String,
    type: String,
    suscripcionEfectiva: String,
    topic: String
});
module.exports = mongoose.model('mensajeauditoria', mensajeAuditoria);