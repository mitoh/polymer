var WebSocket = require('ws'),
    topicController = require("../controller/TopicAuditController"),
    messageController = require("../controller/MessageController"),
    config,
    clients = []

/**
 * Se ocupa de levantar el servidor websocket.
 * manda a manejar los eventos del servidor
 */
exports.levantarServidor = function(){
    const http = require('http');
    
    var webSocketPort = config.ws.port

    log("Levantando servidor ws puerto: " + webSocketPort)

    const server = new http.createServer(function(req, res){
        keepAlive:true
    });
    const wss = new WebSocket.Server({ server });

    handleEvents(wss);

    server.listen(webSocketPort);

    if(server.listening){
      log("Escuhando websokcet en puerto: " + webSocketPort)
    }
}

/**
 * Maneja los eventos producidos por el servidor ws
 * @param {*} wss 
 */
function handleEvents(wss){
    wss.on('connection', function connection(ws) {
        console.log('Conexion creada');
        //Auditar

        ws = asignarId(ws)
    
        ws.on('message', function incoming(message) {
            message = JSON.parse(message)
            log('received: ', message);
    
            var action = message.action
            var value = message.value 
    
            log("Action :" + action)
            log("value: " + value)
            log("Identificador webSocket: " + ws.uuid)
    
            if(action == "toConnect"){
                if(value){
                    var topicClient = topicController.conectarATopico(ws, value, enviarAPolymer);
                    ws.topicClient = topicClient;
                }else{
                    log("No hay valor para la conexion al topico")
                }
            }else if(action == "forSearch"){
                log("Filtro llamado: " + value)
                //TODO: borrar
                value = 1
                if(value){
                    var query = {
                        wsId: {$in: ["618cb890-e20b-11e8-a817-ed65bef187b0"]}, // ws.uuid,
                        //topicId: ws.topicClient.uuid
                    },
                    //filtro = parseFilter(value)
                    // "date gte 07-11-2018, date lte 08-11-2018"
                    // "is = Prueba de MQTT WebSocket"
                    
                    filtro = parseFilter("is  RequerimientoPresupuestarioEJBRemoteBean")
                    //topicClient.filtro = filtro
                    messageController.filtrarMensajes(query, filtro, result => {
                        ws.send(result)
                    })
                }else{
                    console.error("No hay valor para la busqueda.")
                }
            }
    
            //Auditar
        },
        ws.on('close', function close(webSocketCerrado){
            log('Se detecta conexion WS cerrada. Manejando websocket cerrado');
            topicController.handleWSCerrado(ws)
            log('Se ha cerrado la conexion con el topico.');
            log('WebSocket cerrado cerrado!');
            //Auditar
        }));
    
        var response = {
            type: "alert",
            message: "Conexión creada. Recibiendo streaming de Auditoría",
            date : new Date()
        }
        
        ws.send(JSON.stringify(response));
    });
}

/**
 * Funcion que crea objeto json Filtro con los 
 * parametros necesarios para iniciar la busqueda
 * @param {InconmingFilter} inconmingFilter 
 * @returns {Auditfilter} auditFilter - Objecto Json de filtro
 */
function parseFilter(inconmingFilter){
    // "is Prueba de MQTT WebSocket"

    var auditFilter = inconmingFilter.split("|")[0].split(",").map(filterPart => {
        filterPart = filterPart.trim().split(" ")
    
        switch(filterPart[0]){
            case "date":
                filterPart = {
                    filtro: filterPart[0],
                    operador: filterPart[1],
                    predicado: filterPart[2]
                }
                log("Filtro Date reconocido: " + JSON.stringify(filterPart))
                break;
            case "hour":
            case "is":
                var operador = filterPart[1],
                    predicado = "",
                    largo = filterPart.length - 1
                if(operador != "=" && operador != "and" && operador != "or"){
                    
                    if(largo){
                        predicado = ""
                        for(var i=1;i<=largo;i++){
                            predicado += filterPart[i] + " "
                        }
                        predicado = predicado.trim()
                    }else{
                        throw new Error("No existe predicado para el filtro 'is'")
                    }
                    
                    operador = "="
                }else{
                    for(var i=2;i<=largo;i++){
                        predicado += filterPart[i] + " "
                    }
                    predicado = predicado.trim()
                }

                filterPart = {
                    filtro: filterPart[0],
                    operador: operador,
                    predicado: predicado
                }

                log("Filtro is: " + JSON.stringify(filterPart))
                break;
            case "not":
            default:
                throw new Error("No se reconoce el filtro: " + filterPart[0])
        }
        return filterPart;
    })

    log("Filtro parseado: " + JSON.stringify(auditFilter))
    return auditFilter;
}

/**
 * Agrega order a la query si existe
 * @param {*} filtro 
 * @param {*} query 
 */
function insertOrderBy(filtro, query){
    var order = filtro.order;

    if(order){
        query.order = order
    }
    return query;
}


/**
 * Envia mensaje 'sendToPlymer' hacia el cliente webSocket
 * @param {*} topicClient 
 * @param {*} sendToPlymer 
 * @param {*} handleWSCerrado 
 */
function enviarAPolymer(topicClient, sendToPlymer){
    if(topicClient.ws){
        topicClient.ws.forEach(ws => {
            if(ws.readyState === WebSocket.OPEN){
                log("Enviando mensaje a clientes: " + ws.uuid)
                ws.send(sendToPlymer)
            }else{
                topicController.handleWSCerrado(ws)
            }
        });
    }else{
        console.error("No existe ningun cliente conectado al cual enviar el mensaje")
    }
}


/**
 * Setea variables obligatorias para comenzar los flujos
 * @param {*} _topicController 
 * @param {*} _config 
 */
exports.setup = function(_config){

    var error = {
        message: "No se ha seteado componente auditoria websocket adecuadamente"
    }

    if(!_config){
        error.detail = "configuracion"
    }

    if(error.detail){
        console.error(JSON.stringify(error))
        return;
    }

    config = _config
}

/**
 * Asigna identificador al cliente
 * @param {*} ws 
 */
function  asignarId(ws){
    var uuid = require('uuid/v1')();
    ws.uuid = uuid
    return ws;
}


/**
 * Logea en consola la entrad.
 * El segundo parametro debe ser un objeto
 * @param {*} texto 
 * @param {*} jsonEntrada 
 */
function log(texto, jsonEntrada){
    console.log("-----------------------------------------")
    if(texto){
        console.log(texto)
    }
    logJson(jsonEntrada)
}

/**
 * Parsea a String el contenido y logea.
 * El parametro debe ser un objeto.
 * Agrega fecha al objeto al momento de hacer log en variable 'date'
 * @param {Object} jsonEntrada 
 */
function logJson(jsonEntrada){
    if(jsonEntrada && jsonEntrada instanceof Object){
        jsonEntrada.date = new Date();
        console.log(JSON.stringify(jsonEntrada))
    }
    console.log("-----------------------------------------")
}

