
exports.performSearch = (filtro, messages, callBack) => {

    var elasticlunr = require('elasticlunr'),
        messagesTmp = []

    /**
     * Generando indices
     */
    var index = elasticlunr(function () {
        messages.forEach((incommingMessage, index) => {
                var cont = 0,
                    message = incommingMessage.message
                    message.id = index
            
                for(var attr in message){
                    this.addField(attr)
                    this.setRef("id")
                    cont++
                }
                this.addDoc(message)
            
        })
    }); 

    filtro.forEach(incommingFilter => {
        var predicado = incommingFilter.predicado
        var search =  index.search(predicado);

        if(incommingFilter.filtro == "is"){
            search.forEach(incommingSearch => {
                var idDocumentoSeleccionado = incommingSearch.ref,
                    documenSeleccionado =  messages[idDocumentoSeleccionado]
                
                delete documenSeleccionado.message.id
    
                messagesTmp.push(messages[idDocumentoSeleccionado])
            })
        }else if(incommingFilter.filtro == "not"){
            search.forEach(incommingSearch => {
                delete documenSeleccionado.message.id
                var idDocumentoSeleccionado = incommingSearch.ref
                messagesTmp = messages.filter((incommingMessage, index) => {
                    if(incommingSearch.ref != index){
                        return incommingMessage
                    }
                })
            })
        }
        
    })
    console.log("Resultados : " + JSON.stringify(messagesTmp))
    callBack(messagesTmp)
}