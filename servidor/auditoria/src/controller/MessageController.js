var mongoose = require('mongoose'),
    MensajeAuditoria = mongoose.model('mensajeauditoria'),
    fs = require('fs'),
    ini = require('ini'),
    diccionario = ini.parse(fs.readFileSync("./config/diccionario-filtro.ini", "utf-8")),
    LunrController = require("./LunrController")


/**
 * Busca los mensajes en coleccion de auditoria
 * y los filtra/ordena segun parametros
 * @param {*} filtros 
 */
exports.filtrarMensajes = (query, filtro, cb) => {
    log("Buscando mensajes de auditoria con: " + JSON.stringify(query), " filtros: " + JSON.stringify(filtro))
    var options = {}

    if(filtro.order){
        options.order = filtro.order
    }

    var obtenerOperadorDeDiccionario = incomingOperator => {
        var outGoinOperator = diccionario.filtro.dateHour.operadores[incomingOperator]
        if(!outGoinOperator){
            throw new Error("No se encuentra el operador [" + incomingOperator + "] en el diccionario.")
        }

        return outGoinOperator
    }

    filtro.forEach(filterPart => {
        var operador = filterPart.operador,
            predicado = filterPart.predicado
        
        if(filterPart.filtro == "date" || filterPart.filtro == "hour"){
            try{
                predicado = predicado.split("-")
                var date = new Date(predicado[2], predicado[1]-1, predicado[0])
                predicado = date
            }catch(err){
                throw new Error("No se puede parsear la fecha dada en el filtro: " + predicado)
            }

            if(!query.date){
                query.date = {}
            }

            if(operador == "="){
                query.date = predicado
            }else if(diccionario.filtro.dateHour.operadores[operador]){
                query.date[obtenerOperadorDeDiccionario(operador)] = predicado
            }else {
                throw new Error("No se ha podido encontrar el operador para fecha/hora en el diccionario")
            }
        }
    });

    log("Query armada: " + JSON.stringify(query))
    log("options armado: " + JSON.stringify(options))

    MensajeAuditoria.find(query, options, function (err, mensajes) {
        if (err) {
            res.status(500).send(err);
        }
        log("Mensajes encontrados: " + mensajes.length)

        LunrController.performSearch(filtro, mensajes, (result) => {
            cb(JSON.stringify(result))
        })

    });
}

/**
 * Filtra y ordena los elementos
 * @param {*} filtro 
 * @param {*} mensajes 
 * @return mensajes > mensajes ordenados y filtrados
 * 
 * filtro: {
 *  
 * }
 */
function filtrarYOrdenar(filtro, mensajes) {

    var filtros = filtro.filtros,
        orden = filtro.orden


    return mensajes
}

/**
 * Borra los mensajes segun parametro
 * @param {*} query 
 */
exports.borrarMensaje = function (query) {
    log("Intentando borrar mensje con: " + query)
    MensajeAuditoria.remove(query, function (err, grupoServiciosModel) {
        if (err) return err
        return true;
    });
}

/**
 * Guarda el mensaje proveniente del topico
 * @param {*} auditoriaContent 
 */
exports.guardarContenidoAuditoria = function (auditoriaContent) {

    log("mensage de auditoria: " + JSON.stringify(auditoriaContent))
    new MensajeAuditoria(auditoriaContent)
        .save(function (err, mensajeauditoria) {
            if (err) {
                return err;
            }
            return true
        });
}

var log = function (text) {
    if (process.env.NODE_ENV !== "production") {
        console.log(text);
    }
}