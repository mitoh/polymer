var busquedaSimple = 
{
    muestra: {
        level: 'INFO',
        en: "contenido"
    },
    ordena: {
        por: ["level", "httpUri"],
        en: "contenido"
    }
},
busquedaConY = 
{
    muestra: {
        y: {
            level: 'INFO',
            operationName: 'obtenerDocumento'            
        },
        en: "contenido"
    },
    ordena: {
        por: ["contenido"],
        en: "contenido",
        tipo: "desc"
    }
},
busquedaConO = 
{
    muestra: {
        o: {
            level: 'INFO',
            operationName: 'obtenerDocumento'            
        },
        en: "contenido"
    },
    ordena: {
        por: ["contenido"],
        en: "contenido",
        tipo: "asc"
    }
}