var mqtt = require('mqtt'),
    config,
    conexiones = [],
    contentController = require('./MessageController')

/**
 * Conecta a topico enviado como parametro por el webSocket
 * Crea mecanismo para enviar mensaje a cliente websocket
 * 
 * - Se asigna objeto websocket a array en cliente de topico 'topicClient.ws'
 * - Se asigna cliente de topico a propieda de objeto websocket
 * @param {*} ws 
 * @param {*} nombreTopico 
 * @param {*} enviarAPolymer 
 */
exports.conectarATopico = function(ws, nombreTopico, enviarAPolymer){
    var topicClient = conexiones.find(topico => {
        return topico.nombreTopico == nombreTopico
    }),
    jsonLog = {
        nombreTopico : nombreTopico,
        wsId: ws.uuid
    }
    log("Intentando hacer conexion a topico. Recuperando conexion para: ", jsonLog)
    
    if(!topicClient){
        log("No existen conexiones hacia el nodo indicado. Creando una nueva conexion a topico", jsonLog)
        topicClient  = mqtt.connect(config.topic.protocol + '://' + config.topic.host,{username:config.topic.username, password:config.topic.password, clientId:config.topic.clientId})
        topicClient = asignarId(topicClient)
        jsonLog.topicId = topicClient.uuid
        topicClient.on('connect', function () {
            log("Conexion realizada a host: " + config.topic.host + " a ruta: " +nombreTopico);
            topicClient.subscribe(nombreTopico);
            log("Subscripcion realizada hacia: " + nombreTopico)
            asignarWSATopico(ws, topicClient);
            log("Asignacion de websocket a topico realizada")
            log("Guardando topico en conexiones generales", jsonLog)
            topicClient.nombreTopico = nombreTopico
            conexiones.push(topicClient)

            topicClient.on('message', function (topic, message, packet) {
                log("Mensaje desde topico: " + message.toString())
                var date = new Date(),
                sendToPlymer = {
                    type: "message",
                    message: message.toString(),
                    date: date
                },
                toPersist = {
                    type: sendToPlymer.type,
                    date: sendToPlymer.date,
                    topic: topic,
                    suscripcionEfectiva: topicClient.nombreTopico
                },
                messageToPersist;

                try{
                    messageToPersist = JSON.parse(message.toString())
                    toPersist.messageType = "json"
                }catch(err){
                    messageToPersist = message.toString()
                    toPersist.messageType = "text"
                }

                toPersist.message = messageToPersist
                //TODO: Convertir en promesa, solo guardar cuando el despliegue
                // en presentacion haya terminado
                enviarAPolymer(topicClient, JSON.stringify(sendToPlymer))
                persistir(toPersist, topicClient, ws)

                //
            })
            
            topicClient.on('packetreceive', function (topic, message, otro, otroo) {
                console.log()
            })
        })
    }else{
        jsonLog.topicId = topicClient.uuid
        log("Topico existente. Asignando nuevo ws a topico", jsonLog)
        topicClient.ws.push(ws)
    }

    return topicClient;
}

/**
 * Persiste el menage desde el topico
 * @param {*} topicMessage 
 * @param {*} topicClient 
 * @param {*} webSocket 
 */
function persistir(sendToPlymer, topicClient, webSocket){
    var topicMessage = sendToPlymer;
    topicMessage.topicId = topicClient.uuid

    if(topicClient.ws){
        topicMessage.wsId = topicClient.ws.map(ws => {
            return ws.uuid
        })
    }
    
    contentController.guardarContenidoAuditoria(topicMessage)
}


exports.handleWSCerrado = function(webSocketCerrado){
    doHandleWSCerrado(webSocketCerrado)
}

/**
 * Maneja la eliminacion de las conexiones webSocket de los topicos
 * cuando estas se cierran
 */
doHandleWSCerrado = function(webSocketCerrado){
    var topicoAEliminar = conexiones.find(topico => {
        return topico.uuid == webSocketCerrado.topicClient.uuid
    })

    var topicLog = {
        nombreTopico: topicoAEliminar.nombreTopico,
        topicId: topicoAEliminar.uuid,
        wsId: topicoAEliminar.ws[0].uuid
    }

    if(topicoAEliminar.ws.length == 1){
        log("Conexion a topico unica. Localizando topico para cerrar la conexion")
        /**
         * Si el contenido es unico, 
         * el topico se elimina de la 
         * lista de conexiones
         */
        log("Topico localizado. Eliminando topico de conexiones generales",  topicLog)
        var topicoIndex = conexiones.findIndex(topico => topico.uuid == topicoAEliminar.uuid);
        
        if(topicoIndex > -1){
            conexiones.splice(topicoIndex, topicoIndex+1)
            topicoAEliminar.end(true)
            log("Topico elminado de conexiones generales y cerrada su conexion con broker", topicLog)
        }
        
    }else{
        /**
         * Si el contenido es mayor a 1,
         * se elimna el websocket de la lista
         * en el topico
         */
        var index = topicoAEliminar.ws.findIndex(ws)
        topico.ws.splice(index, index+1)
    }
}

/**
 * Asigna variable de webSocket al topico conectado
 * @param {*} ws 
 * @param {*} topicClient 
 */
function asignarWSATopico(ws, topicClient){
    console.log("Asignando WS: " + ws.uuid + " a topico: " + topicClient.uuid)
    if(!topicClient.ws){
        topicClient.ws = [];
    }
    topicClient.ws.push(ws)
}


/**
 * Setea variables obligatorias para comenzar los flujos
 * @param {*} _topicController 
 * @param {*} _config 
 */
exports.setup = function(_config){

    var error = {
        message: "No se ha seteado componente auditoria websocket adecuadamente"
    }

    if(!_config){
        error.detail = "configuracion"
    }

    if(error.detail){
        console.error(JSON.stringify(error))
        return;
    }

    config = _config
}

/**
 * Asigna identificador al cliente
 * @param {*} entity
 */
function  asignarId(entity){
    var uuid = require('uuid/v1')();
    entity.uuid = uuid
    return entity;
}

/**
 * Logea en consola la entrad.
 * El segundo parametro debe ser un objeto
 * @param {*} texto 
 * @param {*} jsonEntrada 
 */
function log(texto, jsonEntrada){
    console.log("-----------------------------------------")
    if(texto){
        console.log(texto)
    }
    logJson(jsonEntrada)
}

/**
 * Parsea a String el contenido y logea.
 * El parametro debe ser un objeto.
 * Agrega fecha al objeto al momento de hacer log en variable 'date'
 * @param {Object} jsonEntrada 
 */
function logJson(jsonEntrada){
    if(jsonEntrada && jsonEntrada instanceof Object){
        jsonEntrada.date = new Date();
        console.log(JSON.stringify(jsonEntrada))
    }
    console.log("-----------------------------------------")
}