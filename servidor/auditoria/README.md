# Servidor de auditoria para Front end

Aplicaicón del lado del servidor que maneja conexiones Websocket con cliente y conexiones a tópicos mediante protocolo mqtt con broker

## Precondiciones: 
- Tener node versión 10.x instalado en el sistema.
- Conexión a servidor de colas mediante mqtt


# Instalación

## Configuración

Modificar variable en `config/audit-config.ini` según su configuración

## Levantar servidor

        node ./auditoriaApp.js

El puerto por para aceptar conexiones con websocket es el `8085`
