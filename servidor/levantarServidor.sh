#!/bin/bash

if test "proxy" == "$1" || test "topic" == "$1" ; then
    echo Levantando aplicacion $1 
    
    if test "proxy" == "$1" ; then
        echo Se genera archivo de log devProxy.log
        nohup npm run devProxy > devProxy.log & 
    fi

    if test "topic" == "$1" ; then
        echo Se genera archivo de log devTopic.log
        nohup npm run devTopic > devTopic.log &  
    fi
else
    echo No se reconce ningun parametro de aplicacion 'proxy' o 'topic'
    echo Levantando aplicaciones Proxy y Topic
    echo Se genera archivo de log devTopic.log
    echo Se genera archivo de log devProxy.log

    nohup npm run devProxy > devProxy.log & 
    nohup npm run devTopic > devTopic.log & 
    
    echo Instancias de Proxy y Topic corriendo.
fi






