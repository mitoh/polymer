process.binding('http_parser').HTTPParser = require('http-parser-js').HTTPParser;

var express = require('express'),
    cors = require('cors'),
    app = express(),
    port = process.env.PORT || 3000,
    mongoose = require('mongoose');
    bodyParser = require('body-parser');
    // //TODO: ordenar

    require('body-parser-xml')(bodyParser);
    require('https').globalAgent.options.ca = require('ssl-root-cas/latest').create();
    process.env['NODE_TLS_REJECT_UNAUTHORIZED'] = '0';


var mongoHost = 'localhost',
    mongoPort = 27017;

var mbLimit = '100mb';
app.use(cors())

/**
 * Conexion mongo
 */
mongoose.Promise = global.Promise;

mongoose.connect('mongodb://'+ mongoHost + ':' + mongoPort + '/services');

/**
 * Librerias
 */
//app.use(bodyParser.urlencoded({ extended: true }));
 //app.use(bodyParser.xml({limit: mbLimit}));
 app.use(bodyParser.json({limit: mbLimit}));
 app.use(bodyParser.urlencoded({limit: mbLimit, extended: true}));


/**
 * Declaracion de modelos.
 * Se deben declarar para que la aplicacion pueda utilizarlos
 */
var GrupoServiciosModel = require('./service-api/src/models/grupoServiciosModel'),
    serviciosModel = require('./service-api/src/models/serviciosModel'),
    CamposServiciosModel = require('./service-api/src/models/camposServiciosModel'),
    ContenidoCamposModel = require('./service-api/src/models/contenidoCamposModel'),
    LoginModel = require('./service-api/src/models/loginModel'),
    AmbienteModel = require('./service-api/src/models/ambienteModel'),
    WraperModel = require('./service-api/src/models/wraperModel');

/**
 * Declaracion de rutas
 */
var configServiciosRoute = require('./service-api/src/routes/configServiciosRoute');
configServiciosRoute(app);



/**
 * Configuracion servidor de topicos
 */


/**
 * configuracion global
 */
app.listen(port);
console.log('Todo listo! servidor RESTful iniciado en puerto: ' + port);