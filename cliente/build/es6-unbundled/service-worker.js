/**
 * Copyright 2016 Google Inc. All rights reserved.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
*/

// DO NOT EDIT THIS GENERATED OUTPUT DIRECTLY!
// This file should be overwritten as part of your build process.
// If you need to extend the behavior of the generated service worker, the best approach is to write
// additional code and include it using the importScripts option:
//   https://github.com/GoogleChrome/sw-precache#importscripts-arraystring
//
// Alternatively, it's possible to make changes to the underlying template file and then use that as the
// new base for generating output, via the templateFilePath option:
//   https://github.com/GoogleChrome/sw-precache#templatefilepath-string
//
// If you go that route, make sure that whenever you update your sw-precache dependency, you reconcile any
// changes made to this original template file with your modified copy.

// This generated service worker JavaScript will precache your site's resources.
// The code needs to be saved in a .js file at the top-level of your site, and registered
// from your pages in order to be used. See
// https://github.com/googlechrome/sw-precache/blob/master/demo/app/js/service-worker-registration.js
// for an example of how you can register this script and handle various service worker events.

/* eslint-env worker, serviceworker */
/* eslint-disable indent, no-unused-vars, no-multiple-empty-lines, max-nested-callbacks, space-before-function-paren, quotes, comma-spacing */
'use strict';

var precacheConfig = [["bower_components/app-layout/app-drawer-layout/app-drawer-layout.html","079d286761f1d091dbe8cafc76ce199a"],["bower_components/app-layout/app-drawer/app-drawer.html","39fc02164278d922f06a12a815aff142"],["bower_components/app-layout/app-header-layout/app-header-layout.html","fb94f4326d321cc284ab65273f3563c7"],["bower_components/app-layout/app-header/app-header.html","305ada3409999055fe3e37aa366efacb"],["bower_components/app-layout/app-layout-behavior/app-layout-behavior.html","09bc22bdba053c05f0e478f37a7fcba6"],["bower_components/app-layout/app-scroll-effects/app-scroll-effects-behavior.html","1ee969ea308114897fbd8ec30875f38e"],["bower_components/app-layout/app-scroll-effects/effects/waterfall.html","8819a9e809201c5ba3d2d40403ede42a"],["bower_components/app-layout/app-toolbar/app-toolbar.html","cac42c92a39fd9611d080d1362905030"],["bower_components/app-layout/helpers/helpers.html","237274971e65a747a047e2687ff004a7"],["bower_components/app-route/app-location.html","bd5e056912b005b4a37253e14dc5a572"],["bower_components/app-route/app-route-converter-behavior.html","80ac2fc0afb9dd16ac225ba3713aa0a3"],["bower_components/app-route/app-route.html","82269d604404d0ecd468207b568d4be6"],["bower_components/bwt-datatable/bwt-datatable-card.html","dbee0fb8ac43ef03f18a8ecebd13260d"],["bower_components/bwt-datatable/bwt-datatable-column.html","e5445bfa24c978806d9e272a86a5d503"],["bower_components/bwt-datatable/bwt-datatable-edit-dialog.html","55d00ac82576c448d221dc1a5a5ea2e9"],["bower_components/bwt-datatable/bwt-datatable.html","808a79b94747f04cd6b2bc83830d3c28"],["bower_components/bwt-datatable/datatable-icons.html","17d5921f7e709eb158e5f158ff00096a"],["bower_components/bwt-datatable/src/collectionHelpers.js","f45ff0fe2a6ce0e71c07fc0559827bed"],["bower_components/bwt-datatable/src/weakCache.js","c7a97ab432df647e75fa3d16ecec1380"],["bower_components/clipboard/dist/clipboard.min.js","3ea3ffe88660510d2d14530494b41611"],["bower_components/font-roboto/roboto.html","3dd603efe9524a774943ee9bf2f51532"],["bower_components/granite-clipboard/granite-clipboard.html","5d350c9ed5cacc9a9ecfb95caa38ac28"],["bower_components/iron-a11y-announcer/iron-a11y-announcer.html","3891d462c464862c5706e114e1abe93b"],["bower_components/iron-a11y-keys-behavior/iron-a11y-keys-behavior.html","de57201642e8aa7eadebad45ca7b35e7"],["bower_components/iron-ajax/iron-ajax.html","0e549efa574d0bdda759893a51390e00"],["bower_components/iron-ajax/iron-request.html","08b1c759435db3caa2b9378ccd686e39"],["bower_components/iron-autogrow-textarea/iron-autogrow-textarea.html","638755357b2232fec7d98c6d8cdab219"],["bower_components/iron-behaviors/iron-button-state.html","2034e45c1e5117b83033713cda6a0b4f"],["bower_components/iron-behaviors/iron-control-state.html","d57c6bfd619425b963c65c312a054ab2"],["bower_components/iron-checked-element-behavior/iron-checked-element-behavior.html","0e28003f171922990b2a8ea1ccb9d130"],["bower_components/iron-collapse/iron-collapse.html","63b719b884fd407b6a7e12d152d99117"],["bower_components/iron-dropdown/iron-dropdown-scroll-manager.html","2c130887355bcec6b7b2ebe193f545ab"],["bower_components/iron-dropdown/iron-dropdown.html","c52449e0659595ee29d557741b24f4f4"],["bower_components/iron-fit-behavior/iron-fit-behavior.html","d017c37e8e343ede515255059b4d78db"],["bower_components/iron-flex-layout/iron-flex-layout-classes.html","7fdc2ab3c7921949621e8374a86e2af4"],["bower_components/iron-flex-layout/iron-flex-layout.html","40fbf9b980a269b5507022f32f9b413c"],["bower_components/iron-form-element-behavior/iron-form-element-behavior.html","0dedf1c9a04536e6dcafac13beee605a"],["bower_components/iron-form/iron-form.html","60c797dbdca4e0db03e9d60ff1e60d2d"],["bower_components/iron-icon/iron-icon.html","86e2b60880947c0b39494a73411fbc11"],["bower_components/iron-icons/communication-icons.html","11ba510515f0e44ff68521fe50a80ef2"],["bower_components/iron-icons/editor-icons.html","845a13dec7e65c3f0f852bf30d950b2d"],["bower_components/iron-icons/iron-icons.html","f167b940536136378cba6ddbc6bb00d0"],["bower_components/iron-iconset-svg/iron-iconset-svg.html","84a7393de41f8ea5da7a599f480b57f0"],["bower_components/iron-image/iron-image.html","0367b237486a99f74bf5ee140e95b3c8"],["bower_components/iron-input/iron-input.html","40bb4bbc62b07540ba593d2cf74e7dca"],["bower_components/iron-localstorage/iron-localstorage.html","9514bca860c52d7da272c71071abfe62"],["bower_components/iron-location/iron-location.html","246e9e57c65edac20efdf755c7f58aa5"],["bower_components/iron-location/iron-query-params.html","68baaf760be6d810068b26c83f8e66ca"],["bower_components/iron-media-query/iron-media-query.html","07eb0b58f4e004bb03453c9b8a673664"],["bower_components/iron-menu-behavior/iron-menu-behavior.html","bb8aada82d13df5b839923fc817484b2"],["bower_components/iron-menu-behavior/iron-menubar-behavior.html","e86b1a7fd638275ca05880ca0f6aa3eb"],["bower_components/iron-meta/iron-meta.html","d3401c6909ebd2a7da37f6bf5fae988b"],["bower_components/iron-overlay-behavior/iron-focusables-helper.html","340b583bc8f50cf5817490e60ca60939"],["bower_components/iron-overlay-behavior/iron-overlay-backdrop.html","05cc5c4d1abbf2d55d73d9b102013191"],["bower_components/iron-overlay-behavior/iron-overlay-behavior.html","2c99ff1debbe68090624b1a52f3f4a50"],["bower_components/iron-overlay-behavior/iron-overlay-manager.html","5902d04f185d2dc6291e0705a7b24725"],["bower_components/iron-overlay-behavior/iron-scroll-manager.html","b824f23f960fad504b5b9562dbd68570"],["bower_components/iron-pages/iron-pages.html","461dc38467532f0a57bf564301bcca78"],["bower_components/iron-range-behavior/iron-range-behavior.html","9f526032f3c5f23f172b1285fc509709"],["bower_components/iron-resizable-behavior/iron-resizable-behavior.html","e8decd555dc3ad8de7532632b13b0ce2"],["bower_components/iron-scroll-target-behavior/iron-scroll-target-behavior.html","8601c5c220d208435e3685dfdc063e2e"],["bower_components/iron-scroll-threshold/iron-scroll-threshold.html","f241c971062393e724d49caa3f8c2ff3"],["bower_components/iron-selector/iron-multi-selectable.html","802945ddfc16eb03e8b605fff57cebb9"],["bower_components/iron-selector/iron-selectable.html","b9248a704cc4895f7ecbccff8efd0edf"],["bower_components/iron-selector/iron-selection.html","30b5a0f391d92c70156b1830fb3bd0c6"],["bower_components/iron-validatable-behavior/iron-validatable-behavior.html","7baac7bb9d9812449b62290a46f070d7"],["bower_components/neon-animation/animations/fade-in-animation.html","749c9d1d5b5f4f27d687fc197309a5c5"],["bower_components/neon-animation/animations/fade-out-animation.html","d68aac80ac6bc94606e236f5eaa405ef"],["bower_components/neon-animation/neon-animatable-behavior.html","110532d0bd679a9fffce01d4085f741d"],["bower_components/neon-animation/neon-animation-behavior.html","7851a2111778abe5f869bb6e1584b20b"],["bower_components/neon-animation/neon-animation-runner-behavior.html","0da4f61f6a232924d2871fe580f1f355"],["bower_components/neon-animation/web-animations.html","aa5266664b17a9a7d7ebf0c4e6fcf8c9"],["bower_components/paper-behaviors/paper-button-behavior.html","ba4f655d100442d73343d6e4f60aa358"],["bower_components/paper-behaviors/paper-checked-element-behavior.html","377ef379e22b072ffcd6374c63b90d62"],["bower_components/paper-behaviors/paper-inky-focus-behavior.html","3b088afa4531829d1a5b79d3bf5978f1"],["bower_components/paper-behaviors/paper-ripple-behavior.html","574608962bf3eb67383391cf8513d56b"],["bower_components/paper-button/paper-button.html","b0c95dacbbf7e1ce20ea182911dcbd34"],["bower_components/paper-card/paper-card.html","cf118e71bee637199ece0a506f3acd17"],["bower_components/paper-checkbox/paper-checkbox.html","c59f48813759864a7e8d4e84510db3ce"],["bower_components/paper-dialog-behavior/paper-dialog-behavior.html","a7a38d677669138125627b5f26c7253f"],["bower_components/paper-dialog-behavior/paper-dialog-shared-styles.html","5b97501e5b6ef42fa957487ea4c88e7e"],["bower_components/paper-dialog-scrollable/paper-dialog-scrollable.html","d51641cd7577076f32ddd5884cbd26ac"],["bower_components/paper-dialog/paper-dialog.html","dbc40043361685fe33f5a64c5a001d66"],["bower_components/paper-dropdown-menu/paper-dropdown-menu-icons.html","bd8d99e625c1baab3431ae830d788c72"],["bower_components/paper-dropdown-menu/paper-dropdown-menu-shared-styles.html","62226dde51d0f26f0ccab279cfb89b58"],["bower_components/paper-dropdown-menu/paper-dropdown-menu.html","b36dd99a9ef58d3f83729bcff741482a"],["bower_components/paper-icon-button/paper-icon-button.html","a557e2789045f5c41da9befed2f6350c"],["bower_components/paper-input/paper-input-addon-behavior.html","a74ff1acb61cb7fc451e13d01d64b0ab"],["bower_components/paper-input/paper-input-behavior.html","2477bf1b481a0c0846f045e98cf35ef4"],["bower_components/paper-input/paper-input-char-counter.html","3ae922107097dd92f27ca6833e346694"],["bower_components/paper-input/paper-input-container.html","c22806c0d4e4f1d6ead2ad3526efe7a2"],["bower_components/paper-input/paper-input-error.html","bc4f6ffdc9de51776c7240e05dbed3a1"],["bower_components/paper-input/paper-input.html","3c42ce89129207a8b51825e55b7dcd86"],["bower_components/paper-input/paper-textarea.html","b42e09902c63ad9f9fe7eea51a91f27b"],["bower_components/paper-item/paper-icon-item.html","e5a84379c6c88dcda71319862231c1da"],["bower_components/paper-item/paper-item-behavior.html","fe3b93f23bb620f4abcb1fa3b8cb0c48"],["bower_components/paper-item/paper-item-body.html","53903cc740e470a5f0661869d89d2f8f"],["bower_components/paper-item/paper-item-shared-styles.html","b5104778f1e5f558777d7558623493db"],["bower_components/paper-item/paper-item.html","bbcea6a06ad2e50f9d46e45adbe58514"],["bower_components/paper-listbox/paper-listbox.html","d33a53b16db2af1e3f40dbcb4116217f"],["bower_components/paper-material/paper-material-shared-styles.html","0880145bd868df7784d5cd49963468f6"],["bower_components/paper-material/paper-material.html","b37f0cb5775746f2443f4c82101fb958"],["bower_components/paper-menu-button/paper-menu-button-animations.html","5d24a43a8fd4c3c1b3a0a1b1fbf106a6"],["bower_components/paper-menu-button/paper-menu-button.html","c097e3240bee3dc2f69763f36d916887"],["bower_components/paper-progress/paper-progress.html","1885e0ba9b8a1cf52c1a903456396346"],["bower_components/paper-ripple/paper-ripple.html","b4cc3ee650f23101e9a4a0be44968a1a"],["bower_components/paper-spinner/paper-spinner-behavior.html","8685ad432fbded77b263aad4a91034e5"],["bower_components/paper-spinner/paper-spinner-styles.html","f6b2d42a9d2262fafb034ea0f802fc80"],["bower_components/paper-spinner/paper-spinner.html","acff8d1e71eaac17569976125462ff67"],["bower_components/paper-styles/color.html","549925227bc04f9c17b52e2e35cd2e26"],["bower_components/paper-styles/default-theme.html","5357609d26772a270098c0e3ebb1bb98"],["bower_components/paper-styles/element-styles/paper-material-styles.html","8d8d619e6f98be2c5d7e49ca022e423c"],["bower_components/paper-styles/paper-styles.html","3a86674df8b40032fc42fe95649bbec6"],["bower_components/paper-styles/shadow.html","1f23a65a20ed44812df26a9c16468e3f"],["bower_components/paper-styles/typography.html","195497070df39ff889ce243627cf6589"],["bower_components/paper-tabs/paper-tab.html","a2cf8ad1d440c0907bee5fb668fd9c5a"],["bower_components/paper-tabs/paper-tabs-icons.html","f8e9e4ba00752fc54f1046143ba1be28"],["bower_components/paper-tabs/paper-tabs.html","c9e19299c83b2089cd56b08cbe3c0998"],["bower_components/paper-toast/paper-toast.html","44750c607be8c6e4e4f9f0560764092a"],["bower_components/paper-tooltip/paper-tooltip.html","2447b1b10516711ef349f826c93ef3ab"],["bower_components/polymer/lib/elements/array-selector.html","841dc72edc195009030cac467dcaccad"],["bower_components/polymer/lib/elements/custom-style.html","08afb86580116b7e4d39d43a39cd1d08"],["bower_components/polymer/lib/elements/dom-bind.html","41004de9dca438cb73383a94fe646d1f"],["bower_components/polymer/lib/elements/dom-if.html","c1fc3b3b3ddd0989b627cb0bfc520cb6"],["bower_components/polymer/lib/elements/dom-module.html","51f4c371c9410959c3feca850c742712"],["bower_components/polymer/lib/elements/dom-repeat.html","8ea3b0cf97eb7232f5f9a561d36115b3"],["bower_components/polymer/lib/legacy/class.html","72a154ebb7232938bdc65e94b13d7508"],["bower_components/polymer/lib/legacy/legacy-element-mixin.html","7b796531a0b47ac74059df0ada681333"],["bower_components/polymer/lib/legacy/mutable-data-behavior.html","727424c73ce82a221dd5d55dae8bfb7e"],["bower_components/polymer/lib/legacy/polymer-fn.html","80b9a95b6f9627267b498fae324c8aec"],["bower_components/polymer/lib/legacy/polymer.dom.html","44aedb235eec8a562cb3ad63bb1033ee"],["bower_components/polymer/lib/legacy/templatizer-behavior.html","e259e4210ec65f4c25459720ce7b71b0"],["bower_components/polymer/lib/mixins/dir-mixin.html","db536a9ada8cdc0fb2fc010e59fbc5e5"],["bower_components/polymer/lib/mixins/element-mixin.html","a2607ad7b0e6e857edf8bb438dbd8030"],["bower_components/polymer/lib/mixins/gesture-event-listeners.html","11c9f3ad714623f52dea07e6afaa2b30"],["bower_components/polymer/lib/mixins/mutable-data.html","0c86e6cf2ad4f58a247cbb4e3b8fe365"],["bower_components/polymer/lib/mixins/properties-changed.html","941485133606f5066c9d713748ca896f"],["bower_components/polymer/lib/mixins/properties-mixin.html","b89faebafe8686dffaeb79a3abc83162"],["bower_components/polymer/lib/mixins/property-accessors.html","7287eb3f0383d7e8da9a3b18e569ed7e"],["bower_components/polymer/lib/mixins/property-effects.html","3f82d74daf72dbfaa8a652e42751c8af"],["bower_components/polymer/lib/mixins/template-stamp.html","30a841e5dc48ec28ae2ec04c071c6205"],["bower_components/polymer/lib/utils/array-splice.html","02e37f7a718cb6724e4c1101fd9fe693"],["bower_components/polymer/lib/utils/async.html","2f5b326d88e8030cd26781095235fd6c"],["bower_components/polymer/lib/utils/boot.html","b60843623cc8cb524686f5c9c77b77e0"],["bower_components/polymer/lib/utils/case-map.html","3348b08018d83d39a4447a6bbaa896af"],["bower_components/polymer/lib/utils/debounce.html","bdb9a2e69ead51e6b8bf27583d040e27"],["bower_components/polymer/lib/utils/flattened-nodes-observer.html","0e34b65431c3aca1e492f459f0f64623"],["bower_components/polymer/lib/utils/flush.html","02cf15aa4ad4cc7edc87d6c5724d2c0f"],["bower_components/polymer/lib/utils/gestures.html","23630718c66b674e8cd0cfd942b2b653"],["bower_components/polymer/lib/utils/html-tag.html","95f4ef70c3d2d142f390a98470c194b4"],["bower_components/polymer/lib/utils/import-href.html","d6093e9c471580c1cb35f7686c772fde"],["bower_components/polymer/lib/utils/mixin.html","5ec7b79aa4871070458783ac7c2980a9"],["bower_components/polymer/lib/utils/path.html","279780f8fac6e7f4048f3895f7a05fda"],["bower_components/polymer/lib/utils/render-status.html","c14138dff3da4d203b9bdca9bd93b929"],["bower_components/polymer/lib/utils/resolve-url.html","5bc2e90748b9845386f19a1eee5d1191"],["bower_components/polymer/lib/utils/settings.html","4f688f5909f8493a10a5012176c911cc"],["bower_components/polymer/lib/utils/style-gather.html","1e10e8f6f06cf5d4f976e3fd905f1252"],["bower_components/polymer/lib/utils/templatize.html","8c31c01b8471caf635004e0ca99a27b1"],["bower_components/polymer/lib/utils/unresolved.html","50b8ec3ab60b6b40f4cf4fc931027b80"],["bower_components/polymer/polymer-element.html","26c3b3b8ee7b81243474c7d95636d157"],["bower_components/polymer/polymer.html","72d557b84da0412316b422d8325ad25c"],["bower_components/shadycss/apply-shim.html","5b73ef5bfcac4955f6c24f55ea322eb1"],["bower_components/shadycss/apply-shim.min.js","f0c8756ae87a4490bf061bb3f71113ad"],["bower_components/shadycss/custom-style-interface.html","7e28230b85cdcc2488e87172c3395d52"],["bower_components/shadycss/custom-style-interface.min.js","f4f9af2608b05de4666cbab79eec95de"],["bower_components/timeu-wizard/timeu-wizard.html","660c478b60e26d9c8ec22b40f848686f"],["bower_components/toggle-icon/toggle-icon.html","79b1e700228e765dfc891eb75061a538"],["bower_components/vaadin-button/vaadin-button.html","ec5dba815707b0f0a12ada400013a50c"],["bower_components/vaadin-control-state-mixin/vaadin-control-state-mixin.html","b5eaa410bcd5f5c453bd3266f630972d"],["bower_components/vaadin-development-mode-detector/vaadin-development-mode-detector.html","95465283513b7302c13058aead9c5739"],["bower_components/vaadin-element-mixin/vaadin-element-mixin.html","51efca5f7c7fecadfbcaafb925bee557"],["bower_components/vaadin-icons/vaadin-icons.html","b58f38268b837c26485eaa7a3bfc8056"],["bower_components/vaadin-lumo-styles/color.html","c9cc1552f5c2017cf1a68e3cfb3800c9"],["bower_components/vaadin-lumo-styles/sizing.html","25ad9e9a8800d45087fd9be497606751"],["bower_components/vaadin-lumo-styles/spacing.html","83d43c70cb2c7cc5214b8b2c32c0a8a0"],["bower_components/vaadin-lumo-styles/style.html","bb0cafcf0c2fc5d5a5b3de32ea017429"],["bower_components/vaadin-lumo-styles/version.html","a921157aeb945f49db639d3e582e1333"],["bower_components/vaadin-progress-bar/src/vaadin-progress-bar.html","4966a96d4215c49038c8b00d14242914"],["bower_components/vaadin-progress-bar/src/vaadin-progress-mixin.html","b4c0b0655f8b4fe03f1651272de016e9"],["bower_components/vaadin-progress-bar/theme/lumo/vaadin-progress-bar.html","bfcb9e3a06de68076609f2858b663aa5"],["bower_components/vaadin-progress-bar/vaadin-progress-bar.html","61ef7294b98705d2e6e24d8e95cb1900"],["bower_components/vaadin-themable-mixin/vaadin-themable-mixin.html","edc41b958d3a312c4f694f563d3dec75"],["bower_components/vaadin-upload/vaadin-upload-file.html","fe2d63afcaa028fcd700edef2deb8636"],["bower_components/vaadin-upload/vaadin-upload-icons.html","2bf82b95619b25c4df9d12b21545ac2c"],["bower_components/vaadin-upload/vaadin-upload.html","6de08c9b70c2032aa2a74e90be8401ab"],["bower_components/web-animations-js/web-animations-next-lite.min.js","6579d2914a8920d46d8cc74a3cff3dec"],["bower_components/websocket-component/shared-websocket-component.html","ede71930870fdc0b23cf9cab1f23b70d"],["bower_components/websocket-component/websocket-component.html","ea77b10f13936a3354f465f1d899c384"],["bower_components/websocket-component/websocket-sharing-behavior.html","30100e6f78c5645b20721033b6a0e6ff"],["bower_components/whenever.js/whenever.js","cdc22db8469cb24321a935aa7a0009a3"],["index.html","bd80f59d9c126e7f77151a8b704a4347"],["src/components/apis/main-api/components/auth-component.html","b721c89d84e3bddd1a99f13abf3b1443"],["src/components/apis/main-api/components/dali-loading-component.html","1c5eec84405e8508bb8f6015e19db1f8"],["src/components/apis/main-api/components/main-component.html","aaa35f2b1203fb7d8e9975dded36d8dd"],["src/components/apis/main-api/components/my-view404.html","c205de3864d0a1f55f5850f4516170d9"],["src/components/apis/service-api/components/config/config-component.html","fbca24bd29c03a6ca06620abb86c5b1d"],["src/components/apis/service-api/components/fuse-rest-call/fuse-rest-call.html","35e9f9b303b2d54cdf2674950bc2e031"],["src/components/apis/service-api/components/historial-servicios/historial-servicios.html","4049eec4efc81279020a3ecee5469561"],["src/components/apis/service-api/components/layout-component/ambiente-component/ambiente-component.html","2e2c4082ee451e97df02815f71405eb6"],["src/components/apis/service-api/components/layout-component/ambiente-component/edit-ambiente-component.html","9089a78f730fe4c18a386f00c51da898"],["src/components/apis/service-api/components/layout-component/ambiente-component/edit-ambiente-layout.html","4a9927d7aa6baa688b7902196160717a"],["src/components/apis/service-api/components/layout-component/auditoria-layout/auditoria-component.html","0df469ece5bb3af25ba1c1d35f3c2256"],["src/components/apis/service-api/components/layout-component/configuracion-servicios/configuracion-grupos.html","8bfc1042f4583c1ec248eb0a164bb949"],["src/components/apis/service-api/components/layout-component/configuracion-servicios/configuracion-servicios-content.html","39a7be654b5efc4500b3c682d1a55c0f"],["src/components/apis/service-api/components/layout-component/configuracion-servicios/configuracion-servicios-layout.html","c0e16d2ec6d7606549f07c4b15c89537"],["src/components/apis/service-api/components/layout-component/configuracion-servicios/edicion-grupos.html","8869de7c095ce8f24889ab07d811521f"],["src/components/apis/service-api/components/layout-component/configuracion-servicios/edicion-servicios.html","bc0fb53bf704dfd56103715390c972cb"],["src/components/apis/service-api/components/layout-component/configuracion-servicios/nuevo-grupo-component.html","0f04c1fa374ff361179c28cd258d259f"],["src/components/apis/service-api/components/layout-component/configuracion-servicios/nuevo-servicio-component.html","3880b12d2cb58a5f1a3d82ec5ef7864f"],["src/components/apis/service-api/components/layout-component/dali/dali-button/dali-button.html","8c9d2da5f9292a9957d83e81062b8cd3"],["src/components/apis/service-api/components/layout-component/dali/dali-check/dali-check.html","e0f8cf2deb90be4b634fd2116272999d"],["src/components/apis/service-api/components/layout-component/dali/dali-file-reader/dali-file-reader.html","90f89f4eba8d5401da1d5541b5be8439"],["src/components/apis/service-api/components/layout-component/dali/dali-icon-button/dali-icon-button.html","336ce8d5eb78bfa32f8cadb764b438e3"],["src/components/apis/service-api/components/layout-component/dali/dali-icon-item/dali-icon-item.html","badfcb0767f21b13fc3ecd0915bf857c"],["src/components/apis/service-api/components/layout-component/dali/dali-icon/dali-icon.html","f99c815c0f5caf9a8c8969fbfb307f4b"],["src/components/apis/service-api/components/layout-component/dali/dali-input/dali-input.html","e431066974013a0afabf1f8d023151ca"],["src/components/apis/service-api/components/layout-component/dali/dali-list/dali-list.html","c6fafff2e85982515723d3371fd4e92e"],["src/components/apis/service-api/components/layout-component/dali/dali-search-box/dali-search-box.html","2237e928422059c78246c6c7be45d9b6"],["src/components/apis/service-api/components/layout-component/dali/dali-show-interact/dali-show-interact.html","58b79d6c55715acf6efa857af0062c1f"],["src/components/apis/service-api/components/layout-component/dali/dali-ticket-search/dali-ticket-content.html","c61cd09020f99221719297c9801301af"],["src/components/apis/service-api/components/layout-component/dali/dali-ticket-search/dali-ticket-search.html","079be92479a0dd354c939749e59d52b1"],["src/components/apis/service-api/components/layout-component/dali/dali-toast/dali-toast.html","49f31ba3baf62d403cdb3a88e1e6ab9b"],["src/components/apis/service-api/components/layout-component/drawer-content.html","dae06f03dfd1152db451ec3bf4161e64"],["src/components/apis/service-api/components/layout-component/historial-layout/historial-layout.html","95df7129ae2263c0fb310314d4a7651b"],["src/components/apis/service-api/components/layout-component/main-content.html","c5f181440dbc7b2c1140abc13fc0d4de"],["src/components/apis/service-api/components/layout-component/service-api-layout.html","460296984b39749982096391d51e94ef"],["src/components/apis/service-api/components/layout-component/sesion-component/sesion-component.html","84ef70d68c9eed6a4254461234057bc2"],["src/components/shared/shared-styles.html","204e70c14453c749cae75e92a7a37f4d"]];
var cacheName = 'sw-precache-v3--' + (self.registration ? self.registration.scope : '');


var ignoreUrlParametersMatching = [/^utm_/];



var addDirectoryIndex = function (originalUrl, index) {
    var url = new URL(originalUrl);
    if (url.pathname.slice(-1) === '/') {
      url.pathname += index;
    }
    return url.toString();
  };

var cleanResponse = function (originalResponse) {
    // If this is not a redirected response, then we don't have to do anything.
    if (!originalResponse.redirected) {
      return Promise.resolve(originalResponse);
    }

    // Firefox 50 and below doesn't support the Response.body stream, so we may
    // need to read the entire body to memory as a Blob.
    var bodyPromise = 'body' in originalResponse ?
      Promise.resolve(originalResponse.body) :
      originalResponse.blob();

    return bodyPromise.then(function (body) {
      // new Response() is happy when passed either a stream or a Blob.
      return new Response(body, {
        headers: originalResponse.headers,
        status: originalResponse.status,
        statusText: originalResponse.statusText
      });
    });
  };

var createCacheKey = function (originalUrl, paramName, paramValue,
                           dontCacheBustUrlsMatching) {
    // Create a new URL object to avoid modifying originalUrl.
    var url = new URL(originalUrl);

    // If dontCacheBustUrlsMatching is not set, or if we don't have a match,
    // then add in the extra cache-busting URL parameter.
    if (!dontCacheBustUrlsMatching ||
        !(url.pathname.match(dontCacheBustUrlsMatching))) {
      url.search += (url.search ? '&' : '') +
        encodeURIComponent(paramName) + '=' + encodeURIComponent(paramValue);
    }

    return url.toString();
  };

var isPathWhitelisted = function (whitelist, absoluteUrlString) {
    // If the whitelist is empty, then consider all URLs to be whitelisted.
    if (whitelist.length === 0) {
      return true;
    }

    // Otherwise compare each path regex to the path of the URL passed in.
    var path = (new URL(absoluteUrlString)).pathname;
    return whitelist.some(function (whitelistedPathRegex) {
      return path.match(whitelistedPathRegex);
    });
  };

var stripIgnoredUrlParameters = function (originalUrl,
    ignoreUrlParametersMatching) {
    var url = new URL(originalUrl);
    // Remove the hash; see https://github.com/GoogleChrome/sw-precache/issues/290
    url.hash = '';

    url.search = url.search.slice(1) // Exclude initial '?'
      .split('&') // Split into an array of 'key=value' strings
      .map(function (kv) {
        return kv.split('='); // Split each 'key=value' string into a [key, value] array
      })
      .filter(function (kv) {
        return ignoreUrlParametersMatching.every(function (ignoredRegex) {
          return !ignoredRegex.test(kv[0]); // Return true iff the key doesn't match any of the regexes.
        });
      })
      .map(function (kv) {
        return kv.join('='); // Join each [key, value] array into a 'key=value' string
      })
      .join('&'); // Join the array of 'key=value' strings into a string with '&' in between each

    return url.toString();
  };


var hashParamName = '_sw-precache';
var urlsToCacheKeys = new Map(
  precacheConfig.map(function (item) {
    var relativeUrl = item[0];
    var hash = item[1];
    var absoluteUrl = new URL(relativeUrl, self.location);
    var cacheKey = createCacheKey(absoluteUrl, hashParamName, hash, false);
    return [absoluteUrl.toString(), cacheKey];
  })
);

function setOfCachedUrls(cache) {
  return cache.keys().then(function (requests) {
    return requests.map(function (request) {
      return request.url;
    });
  }).then(function (urls) {
    return new Set(urls);
  });
}

self.addEventListener('install', function (event) {
  event.waitUntil(
    caches.open(cacheName).then(function (cache) {
      return setOfCachedUrls(cache).then(function (cachedUrls) {
        return Promise.all(
          Array.from(urlsToCacheKeys.values()).map(function (cacheKey) {
            // If we don't have a key matching url in the cache already, add it.
            if (!cachedUrls.has(cacheKey)) {
              var request = new Request(cacheKey, {credentials: 'same-origin'});
              return fetch(request).then(function (response) {
                // Bail out of installation unless we get back a 200 OK for
                // every request.
                if (!response.ok) {
                  throw new Error('Request for ' + cacheKey + ' returned a ' +
                    'response with status ' + response.status);
                }

                return cleanResponse(response).then(function (responseToCache) {
                  return cache.put(cacheKey, responseToCache);
                });
              });
            }
          })
        );
      });
    }).then(function () {
      
      // Force the SW to transition from installing -> active state
      return self.skipWaiting();
      
    })
  );
});

self.addEventListener('activate', function (event) {
  var setOfExpectedUrls = new Set(urlsToCacheKeys.values());

  event.waitUntil(
    caches.open(cacheName).then(function (cache) {
      return cache.keys().then(function (existingRequests) {
        return Promise.all(
          existingRequests.map(function (existingRequest) {
            if (!setOfExpectedUrls.has(existingRequest.url)) {
              return cache.delete(existingRequest);
            }
          })
        );
      });
    }).then(function () {
      
      return self.clients.claim();
      
    })
  );
});


self.addEventListener('fetch', function (event) {
  if (event.request.method === 'GET') {
    // Should we call event.respondWith() inside this fetch event handler?
    // This needs to be determined synchronously, which will give other fetch
    // handlers a chance to handle the request if need be.
    var shouldRespond;

    // First, remove all the ignored parameters and hash fragment, and see if we
    // have that URL in our cache. If so, great! shouldRespond will be true.
    var url = stripIgnoredUrlParameters(event.request.url, ignoreUrlParametersMatching);
    shouldRespond = urlsToCacheKeys.has(url);

    // If shouldRespond is false, check again, this time with 'index.html'
    // (or whatever the directoryIndex option is set to) at the end.
    var directoryIndex = '';
    if (!shouldRespond && directoryIndex) {
      url = addDirectoryIndex(url, directoryIndex);
      shouldRespond = urlsToCacheKeys.has(url);
    }

    // If shouldRespond is still false, check to see if this is a navigation
    // request, and if so, whether the URL matches navigateFallbackWhitelist.
    var navigateFallback = 'index.html';
    if (!shouldRespond &&
        navigateFallback &&
        (event.request.mode === 'navigate') &&
        isPathWhitelisted(["\\/[^\\/\\.]*(\\?|$)"], event.request.url)) {
      url = new URL(navigateFallback, self.location).toString();
      shouldRespond = urlsToCacheKeys.has(url);
    }

    // If shouldRespond was set to true at any point, then call
    // event.respondWith(), using the appropriate cache key.
    if (shouldRespond) {
      event.respondWith(
        caches.open(cacheName).then(function (cache) {
          return cache.match(urlsToCacheKeys.get(url)).then(function (response) {
            if (response) {
              return response;
            }
            throw Error('The cached response that was expected is missing.');
          });
        }).catch(function (e) {
          // Fall back to just fetch()ing the request if some unexpected error
          // prevented the cached response from being valid.
          console.warn('Couldn\'t serve response for "%s" from cache: %O', event.request.url, e);
          return fetch(event.request);
        })
      );
    }
  }
});







