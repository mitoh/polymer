// Node.js notation for importing packages
var express = require('express');

// Spin up a server
var app = express();

// Serve static files from the main build directory
app.use(express.static(__dirname + '/build/es5-bundled'));

console.log("Estos son los args: " +  JSON.stringify(process.argv));

// Render index.html on the main page, specify the root
app.get('/', function(req, res){
    res.sendFile("index.html", {root: '.'});
});

// Tell the app to listen for requests on port 
var port = process.argv[2]
if(port){
    app.listen(port, function () {
        console.log('Cliente polymer corriendo en puerto:' + port);
    });
}else{
    console.error("No se ha especificado un puerto")
}