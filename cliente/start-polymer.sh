#!/bin/bash
ambiente=$1
puerto=$2
egg=""
 

## Si el peruto es nulo
if [ -z "$puerto" ] ; then
    echo Puerto vacío, utilizando por defecto 3001
    puerto = 3001
    echo ambiente: "$ambiente"
fi

if test "dev" == "$ambiente" || test "prod" == "$ambiente" ; then
    echo Reconociendo ambiente $1 
else
    echo Introduzca un ambiente valido: dev o prod

    exit 0;
fi

echo Modificando variables para ambiente seleccionado

if test "prod" == "$ambiente" ; then
    echo Usando variables de Produccion
    echo Afinando las naves para saltar al espacio

    cp ./build/es5-bundled/src/components/apis/service-api/components/config/config/config_prod.json  ./build/es5-bundled/src/components/apis/service-api/components/config/config/config.json
    echo Proceso completado config.json renombrado
      
elif test "dev" == "$ambiente" ; then
    echo Usando variables de Desarrollo

    cp ./build/es5-bundled/src/components/apis/service-api/components/config/config/config_dev.json  ./build/es5-bundled/src/components/apis/service-api/components/config/config/config.json
    echo Proceso completado config.json renombrado 
else
    echo Introduzca un parametro valido dev o prod

    exit 0;
fi

if test "ZGFsaQ==" == "$egg"; then
    echo 
fi

echo
echo Redirigiendo salida a: appPolymer-"$ambiente"-"$puerto".log

nohup node appPolymer.js $puerto > appPolymer-"$ambiente"-"$puerto".log & 
