# Consola INTEROPERABILIDAD

Consola para el manejo de eventos.
    - Api service: Aplicación para administrar los llamados a los servicios de Interoperabilidad

# Notas de la version
- Se crea version beta de los campos dependientes
  - Se da soporte solamente para campos de texto. Próximamente se dará soporte para listas y archivos.
- Versión actual: 0.25 

&nbsp;

&nbsp;

---
# Instalación
---
### Precondiciones
- Contar con una conexión a internet
- Permisos necesarios para instalar software
- Tener instalado GIT, si no lo está, puede hacerlo desde aquí: https://git-scm.com/download/linux
- Usuario GIT con permisos para proyecto: `consola-interoperabilidad`
- Para la utilización por defecto del software es necesario tener un usuario con rol: `IOP_CONSOLE_OP`

`Se recomienda de igual usar un usuario distinto para la instalación de cada producto`

### Instalar base de datos
La base de datos que utilizaremos es MongoDB.
La versión que utilizaremos es ´linux/mongodb-linux-x86_64-rhel70-3.6.5.tgz´ aunque se puede utilizar la que más se acomode al sistema operativo que sea posterior a 3.6.x

1. Descargar mongo desde:  [http://downloads.mongodb.org/linux/mongodb-linux-x86_64-rhel70-3.6.5.tgz](http://downloads.mongodb.org/linux/mongodb-linux-x86_64-rhel70-3.6.5.tgz?_ga=2.215465556.2110185018.1532442169-233926902.1527691223)

2. Descomprimir en /u01/middleware/consola-interoperabilidad/db. Si la carpeta no existe, debe ser creada. La ruta descomprimida debiera ser como esta:

        /u01/middleware/consola-interoperabilidad/db/mongodb-linux-x86_64-rhel70-3.6.5/

3. Agregar variable de entorno ´MONGO_HOME´ apuntando a la carpeta del punto anterior. En este ejemplo, editando ~/.bashrc:

        vi ~/.bashrc
    
    Agregar al final del archivo
    
        export MONGO_HOME="/u01/middleware/consola-interoperabilidad/db/mongodb-linux-x86_64-rhel70-3.6.5/"
        
    Agregar variable al PATH. Agregar al final del archivo:

        export PATH="$PATH:$MONGO_HOME/bin"

4. Agregar reglas al firewall para que puerto por defecto de mongo (27017) pueda ser visto desde la red, ejecutar con usuario root:

        firewall-cmd --zone=public --add-port=27017/tcp --permanent
        firewall-cmd --reload


5. Crear carpeta /data/db. Ejecutar como root:
    
        mkdir -p /data/db


6. Ir a /u01/middleware/consola-interoperabilidad/db/mongodb-linux-x86_64-rhel70-3.6.5/bin/ y ejecutar con usuario de producto:

        nohup ./mongod > mongo.log &

    `Más información del ejecutable ´mongod´ en:` https://docs.mongodb.com/manual/reference/program/mongod/

7. Crear índice para la búsqueda de historial: Ir a /u01/middleware/consola-interoperabilidad/db/mongodb-linux-x86_64-rhel70-3.6.5/bin/ y ejecutar comando mongo
        
        /u01/middleware/consola-interoperabilidad/db/mongodb-linux-x86_64-rhel70-3.6.5/bin/mongo

    Una vez adentro del prompt escribir lo siguiente:

        use services
        db.llamadaservicios.createIndex({fecha:1})

    Si ha saido todo bien, debería dar una respuesta como esta:

        {
	        "createdCollectionAutomatically" : false,
	        "numIndexesBefore" : 1,
	        "numIndexesAfter" : 2,
	        "ok" : 1
        }

### Instalar servidor NODE
1. Descargar e instalar Node 8.10.0. Descargar desde: https://nodejs.org/download/release/v8.10.0/

2. Descomprimir versión en una carpeta del sistema. Ej: /u01/middleware/consola-interoperabilidad/node. Debería quedar como la siguiente ruta:

        /u01/middleware/consola-interoperabilidad/node/node-v8.10.0-linux-x64

3. Agregar variable de entorno ´NODE_HOME´ apuntando a la carpeta del punto anterior. En este ejemplo, editando ~/.bashrc:

        vi ~/.bashrc

    agregar al final del archivo:

        export NODE_HOME="/u01/middleware/consola-interoperabilidad/node/node-v8.10.0-linux-x64"
    Agregar variable al PATH. Agregar al final del archivo:

        export PATH="$PATH:$NODE_HOME/bin"

4. Seleccionar y dirigirse a una ruta en donde se descargarán los fuentes (Si no existe debe ser creada):

        cd /u01/middleware/consola-interoperabilidad/fuentes/node

5. Clonar fuentes:

    Iniciar repositorio:

        git init

    Agregar origen: 

        git remote add origin -f http://mvn.sigfe.gob.cl/arquitectura/consola-interoperabilidad.git

    Setear sparse checkout en `true`. Indica que se clonará sólo una carpeta:

        git config core.sparsecheckout true

    Se selecciona el nombre de la carpeta a clonar, en este caso `servidor`:

        echo "servidor/*" >> .git/info/sparse-checkout

    Traer los fuentes a la carpeta local:

        git pull origin release

7. Modificar host y puerto apuntando a la db Mongo previamente instalada. Editar archivo appProxy.js

        vi /u01/middleware/consola-interoperabilidad/fuentes/node/servidor/appProxy.js
    Ubicar variables ´mongoHost´ y ´mongoPort´ y modificar según la base de datos configurada en el punto 5. Por ejemplo:
     `mongoHost: Ip del servidor en donde corre MongoDB`
    `mongoPort: Puerto en donde corre MongoDB. Normalmente 27017`

        mongoHost = 'localhost'
        mongoPort = 27017
  
8. Agregar reglas a firewall para que el puerto 3000 pueda ser visto desde la red, ejecutar con usuario root:

        firewall-cmd --zone=public --add-port=3000/tcp --permanent
        firewall-cmd --reload

    Para verificar la configuración del firewall:

        iptables-save | grep 3000
    output: -A IN_public_allow -p tcp -m tcp --dport 3000 -m conntrack --ctstate NEW -j ACCEPT

10. Dar parmisos de ejecución. Ejecutar como root:

        cd /u01/middleware/console-interoperabilidad/fuentes/node/servidor
        chmod 0755 levantarServidor.sh
    

11. Ir a ruta y correr node. Listo! solo falta ejecutar lo siguiente con usuario de producto:
        
        ./levantarServidor.sh
    `Nota: Existen dos aplicaciones capaces de correr con este comando: appProxy y appTopic que son las que llevan y traen información desde y hacia los servicios y la que levanta el servidor capaz de conectarse con los módulos de auditoría respectivamente. Si se quisiera levantar sólo uno de ellos agregar "proxy" o "topic" luego del comando ./levantarServidor.sh. Si no se agrega ninguna opción, el sistema levantará ambos`

    La consola debiera mostrar algo como esto:

        Todo listo! servidor RESTful iniciado en puerto: 3000

12. Dar parmisos de ejecución. Ejecutar como root:

        chmod 0755 importServices.sh
    
13. Importar data. 

    Ir a carpeta:
    
        /u01/middleware/consola-interoperabilidad/fuentes/node/servidor/db/

    Modificar archivo `importServices.sh` para indicar el host y puerto de la DB de destino. Una vez hecho, ejecutar con usuario de producto:
    
        ./importServices.sh

14. Cambiar host de todos los servicios. (Paso opcional)

    Ejecutar:
    
        /u01/middleware/consola-interoperabilidad/fuentes/node/servidor/db/setHost.sh

    *Nota: Antes de ejecutar este comando compruebe que el script apunta al host necesario. Por defecto apunta a localhost*

### Instalar cliente > Aplicación Polymer
Estas instrucciones lo ayudarán a instalar la aplicación de interoperabilidad por el lado del cliente.

`Tener en consideración las precondiciones de instalación situadas al comienzo de esta memoria`

1. Clonar código desde repositorio GIT. Escoger una carpeta para descargar el código, en este ejemplo nos dirigimos a (Si la carpeta no existe, debe ser creada): 

        cd /u01/middleware/console-interoperabilidad/fuentes/polymer

2. Ejecutar comandos para descargar código desde el repositorio GIT:

    Iniciar repositorio:

        git init

    Agregar origen:

        git remote add origin -f http://mvn.sigfe.gob.cl/arquitectura/consola-interoperabilidad.git

    Setear sparse checkout en `true`. Indica que se clonará sólo una carpeta:

        git config core.sparsecheckout true

    Se selecciona el nombre de la carpeta a clonar, en este caso `cliente`:

        echo "cliente/*" >> .git/info/sparse-checkout

    Traer los fuentes a la carpeta local:

        git pull origin release

3. Agregar reglas al firewall para que puerto por defecto de mongo (27017) pueda ser visto desde la red, ejecutar con usuario root:

        firewall-cmd --zone=public --add-port=3001/tcp --permanent
        firewall-cmd --reload
 
4. Dar parmisos de ejecución. Ejecutar como root:

        chmod 0755 /u01/middleware/console-interoperabilidad/fuentes/polymer/cliente/start-polymer.sh
   
5. Modificar consulta servidor node (paso opcional. Aplica solamente cuando el servidor no de no esté en el mismo host que el cliente):
   
        sed -i 's/<ANTIGUO_HOST>/<NUEVO_HOST>/g' /u01/middleware/consola-interoperabilidad/fuentes/polymer/cliente/src/components/apis/service-api/components/config/config/config.json
        

    *`ANTIGUO_HOST = Último host configurado, si no se ha lanzado este comando antes, utilizar valor original 'localhost'`*

    *`NUEVO_HOST = Host destino servidor node`*

6. Correr cliente:

    Ya en la carpeta /u01/middleware/consola-interoperabilidad/fuentes/polymer/cliente, ejecutar con usuario de producto:

        ./start-polymer.sh prod
    `Nota: prod hace referencia al ambiente.`

    *Nota: El servidor está configurado para correr sobre el puerto 3001. Si se desea cambiar, modificar el archivo `package.json` y modificar línea: "console": "polymer serve --port `3001`" al puerto desesado.*

7. En navegador ir a la siguiente url (Ejemplo: http://192.168.244.13:3001):

        <hostServidor>:3001

&nbsp;

---

# Road Map
- Crear micro api express en node
  - `Crear esquemas de base de datos`
  - `Crear rutas -> servicios`
  - `Crear controladores`
  - `Crear servicio común en node para hacer las las llamadas desde polymer`
  - `Llamadas https`
  - `Lógica de negocio`
  - `Lógica conexión con tópicos a través de mqtt`
  - `Corrección errores y casos de borde`
- Creación app polymer -> Service-Api
  - `Diseño arquitectura`
  - Páginas
    - `Autenticación`
    -`Dalí -> extraer en componente`
    - `Dali-search-box`
      - `Componente grafico`
      - `Busqueda dinamica`
    - `dali-button`
    - `dali-input-text`
    -`Dali-file-reader`
      - `Visualizar documento atachado`
    - `Dali-list`
  - Auditoría
    - `Creación de componente en servidor que conecte con servidor de tópicos`
    - `Creación webSocket para transmisión “en vivo” de datos`
    - Creación componente en cliente para conectarse a servidor node
      - `Conectar / Desconectar de servidor`
      - `Conectar / desconectar a tópicos disponibles en árbol`
        - Según perfilamiento
      - `Recibir información desde tópicos hacia presentación (Creación del componente gráfico que muestra la información de los tópicos)`
        - Búsqueda por filtros (tipo splunk)
          - `Versión básica`
          - Versión avanzada
    - Creación modelo auditoría
      - Esquemas de db
      - Lógica para mostrar árbol de auditorías por perfil
  - `Llamada a los servicios`
    - `Respuesta cuando la autenticación falle -> Modificación servicio fuse`
  - Gestionar respuestas
    - `Primera versión`
      - `Reconocer ticket`
        - `Mostrar opciones para copiar y buscar ticket en forma rápida`
      - `Descargar respuestas`
        - `Mostrar diálogo para escoger qué de la respuesta se desea descargar`
          - `Opciones para descargar en txt y xml`
          - `Descargar una parte`
          - `Descargar toda la respuesta`
    - Segunda versión
      - Usabilidad para mostrar respuestas de cualquier tipo
        - Reconocer ticket y mostrar opciones dentro de la respuesta
  - Reordenar y renombrar componentes
    - `Fase 1`
    - Fase 2 
    - Fase 3
  - `Componete para seleccionar el ambiente de destino`
  - `Ver ticker`
  - Administración de servicios
    - puesta en marcha todos los servicios
  - Histórico llamados por servicios
    - `Muestra de datos en tabla con llamada, headers, payload y respuestas`
    - `Paginación`
    - `Agregar filtros básicos`
    - Agregar filtros avanzados
    - Agregar opción para hacer una llamada con los datos del histórico
  - Usabilidad y diseño
  - Corrección errores y casos de borde
  - Agregar soporte para más de un FileReader
  - Documentación
---
**Troubleshooting**
Para suplir errores de versiones en node, ejecutar:

        node app.js